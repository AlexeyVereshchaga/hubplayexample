package com.hubplay.network;

/**
 * 21.04.16.
 *
 * @author Alexey Vereshchaga
 */
public enum Server {
    PROD(""),
    DEV("http://52.38.188.119:8080/");
    private String url;

    Server(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }
}
