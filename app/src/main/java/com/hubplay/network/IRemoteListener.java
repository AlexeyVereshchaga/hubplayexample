package com.hubplay.network;

import retrofit2.Call;

/**
 * 16.02.16.
 *
 * @author Alexey Vereshchaga
 */
public interface IRemoteListener<T, ERROR> {

    void onStartTask();

    void onSuccess(T result);

    void onFailure(ERROR error);

    void onError(Throwable t);

    Boolean onFailure(Integer failureCode, ERROR error, Call<T> call, BaseHandler<T, ERROR> handler);

    void onFinishTask();
}
