package com.hubplay.network;

import retrofit2.Call;

/**
 * 25.05.16.
 *
 * @author Alexey Vereshchaga
 */
public class WrapperCall {
    private Call call;
    private BaseHandler handler;

    public WrapperCall(Call call, BaseHandler handler) {
        this.call = call;
        this.handler = handler;
    }

    public Call getCall() {
        return call;
    }

    public BaseHandler getHandler() {
        return handler;
    }
}
