package com.hubplay.network;

import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

import com.hubplay.controller.DataManager;
import com.hubplay.network.model.ErrorModel;
import com.hubplay.utils.Constant;
import com.hubplay.utils.HelpUtils;

import java.net.HttpURLConnection;

import retrofit2.Call;

public abstract class BaseRemoteListener<T, ERROR> implements IRemoteListener<T, ERROR> {

    protected static final String FAILED_TO_CONNECT = "Failed to connect";
    public static final String INVALID_TOKEN = "invalid_token";

    protected FragmentActivity activity;

    private static final String TAG = BaseRemoteListener.class.getSimpleName();

    public BaseRemoteListener(FragmentActivity activity) {
        this.activity = activity;
    }

    @Override
    public void onStartTask() {
    }

    @Override
    public void onSuccess(T result) {
    }

    @Override
    public void onFailure(ERROR error) {
        if (activity != null) {
            String errorMessage = null;
            try {
                errorMessage = ((ErrorModel) error).getErrorDescription();
            } catch (Exception e) {
                Log.e(TAG, "Error output", e);
            }
            if (errorMessage == null || errorMessage.isEmpty()) {
                errorMessage = "Error";
            }
            Toast.makeText(activity, errorMessage, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onFinishTask() {
    }

    @Override
    public Boolean onFailure(Integer failureCode, ERROR error, Call<T> call, BaseHandler<T, ERROR> handler) {
        ErrorModel errorModel = null;
        try {
            errorModel = (ErrorModel) error;
        } catch (Exception e) {
            Log.e(TAG, "onFailure: ", e);
        }
        if (HttpURLConnection.HTTP_UNAUTHORIZED == failureCode
                && errorModel != null
                && errorModel.getError().equals(INVALID_TOKEN)
                && call != null
                && handler != null) {
            WrapperCall wrapperCall = new WrapperCall(call, handler);
            DataManager.addCallUnauthorized(wrapperCall);
            DataManager.refreshToken(activity);
            return true;
        }
        return false;
    }

    @Override
    public void onError(Throwable t) {
        if (t != null) {
            Log.e(TAG, "", t);
            if (t.getMessage() != null && t.getMessage().contains(FAILED_TO_CONNECT)) {
                if (activity != null && activity.getFragmentManager().findFragmentByTag(Constant.OFFLINE_DIALOG_TAG) == null) {
                    HelpUtils.showOfflineDialog(activity);
                }
            }
        }
    }
}
