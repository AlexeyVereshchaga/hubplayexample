package com.hubplay.network;

import com.hubplay.network.model.request.Registration;
import com.hubplay.network.model.response.Access;
import com.hubplay.network.model.response.CommonEventsModel;
import com.hubplay.network.model.response.Event;
import com.hubplay.network.model.response.League;
import com.hubplay.network.model.response.QuestionOrder;
import com.hubplay.network.model.response.Reward;
import com.hubplay.network.model.response.User;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;


/**
 * 21.04.16.
 *
 * @author Alexey Vereshchaga
 */
public interface HubPlayApi {
    String VERSION_PREFIX = "v1/";

    @FormUrlEncoded
    @POST("oauth/token")
    Call<Access> getToken(@Field("grant_type") String grantType,
                          @Field("username") String username,
                          @Field("password") String password,
                          @Field("client_id") String clientId,
                          @Field("client_secret") String clientSecret,
                          @Field("refresh_token") String refreshToken);

    @GET(VERSION_PREFIX + "user/user")
    Call<User> getUser();

    @FormUrlEncoded
    @POST(VERSION_PREFIX + "user/forgotpwd")
    Call<Void> forgotPassword(@Field("email") String email);

    @POST(VERSION_PREFIX + "/user/register")
    Call<Void> register(@Body Registration registration);

    @GET(VERSION_PREFIX + "leagues/leagues")
    Call<List<League>> getLeagues();

    @GET(VERSION_PREFIX + "leagues/full_leagues")
    Call<List<League>> getFullLeagues();

    @GET(VERSION_PREFIX + "events/events/{leagueId}")
    Call<List<Event>> getEvents(@Path("leagueId") Integer leagueId);

    @GET(VERSION_PREFIX + "events/event_by_day/{leagueId}/{page}")
    Call<List<Event>> getEventsByDay(@Path("leagueId") Integer leagueId, @Path("page") Integer page);

    @FormUrlEncoded
    @POST(VERSION_PREFIX + "gameflow/save_gameflow")
    Call<Void> saveGameFlow(@Field("answerId") Integer answerId,
                            @Field("points") Integer points,
                            @Field("questionId") Integer questionId,
                            @Field("eventId") Integer eventId,
                            @Field("isLast") Boolean isLast);

    @FormUrlEncoded
    @POST(VERSION_PREFIX + "user/event/history/action")
    Call<QuestionOrder> sendGameFlowAction(@Field("eventId") Integer eventId,
                                           @Field("action") String action);

    @GET(VERSION_PREFIX + "reward/all")
    Call<List<Reward>> getRewards();

    @GET(VERSION_PREFIX + "events/events_by_category/{category}/{page}")
    Call<CommonEventsModel> getEventsByCategory(@Path("category") String category, @Path("page") Integer page, @Query("league_id") Integer leagueId);

    @GET(VERSION_PREFIX + "user/event/history/clean")
    Call<Void> cleanHistory();
}
