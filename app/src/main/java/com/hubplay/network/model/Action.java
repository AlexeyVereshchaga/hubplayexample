package com.hubplay.network.model;

/**
 * 21.06.16.
 *
 * @author Alexey Vereshchaga
 */
public enum Action {
    STARTED, FINISHED
}
