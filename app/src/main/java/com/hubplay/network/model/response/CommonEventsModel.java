package com.hubplay.network.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hubplay.network.model.ErrorModel;

import java.util.ArrayList;
import java.util.List;


public class CommonEventsModel extends ErrorModel {

    @JsonProperty("content")
    private List<Event> content = new ArrayList<>();
    @JsonProperty("last")
    private Boolean last;
    @JsonProperty("totalPages")
    private Integer totalPages;
    @JsonProperty("totalElements")
    private Integer totalElements;
    @JsonProperty("sort")
    private List<Sort> sort = new ArrayList<>();
    @JsonProperty("numberOfElements")
    private Integer numberOfElements;
    @JsonProperty("first")
    private Boolean first;
    @JsonProperty("size")
    private Integer size;
    @JsonProperty("number")
    private Integer number;

    /**
     * @return The content
     */
    @JsonProperty("content")
    public List<Event> getContent() {
        return content;
    }

    /**
     * @param content The content
     */
    @JsonProperty("content")
    public void setContent(List<Event> content) {
        this.content = content;
    }

    /**
     * @return The last
     */
    @JsonProperty("last")
    public Boolean getLast() {
        return last;
    }

    /**
     * @param last The last
     */
    @JsonProperty("last")
    public void setLast(Boolean last) {
        this.last = last;
    }

    /**
     * @return The totalPages
     */
    @JsonProperty("totalPages")
    public Integer getTotalPages() {
        return totalPages;
    }

    /**
     * @param totalPages The totalPages
     */
    @JsonProperty("totalPages")
    public void setTotalPages(Integer totalPages) {
        this.totalPages = totalPages;
    }

    /**
     * @return The totalElements
     */
    @JsonProperty("totalElements")
    public Integer getTotalElements() {
        return totalElements;
    }

    /**
     * @param totalElements The totalElements
     */
    @JsonProperty("totalElements")
    public void setTotalElements(Integer totalElements) {
        this.totalElements = totalElements;
    }

    /**
     * @return The sort
     */
    @JsonProperty("sort")
    public List<Sort> getSort() {
        return sort;
    }

    /**
     * @param sort The sort
     */
    @JsonProperty("sort")
    public void setSort(List<Sort> sort) {
        this.sort = sort;
    }

    /**
     * @return The numberOfElements
     */
    @JsonProperty("numberOfElements")
    public Integer getNumberOfElements() {
        return numberOfElements;
    }

    /**
     * @param numberOfElements The numberOfElements
     */
    @JsonProperty("numberOfElements")
    public void setNumberOfElements(Integer numberOfElements) {
        this.numberOfElements = numberOfElements;
    }

    /**
     * @return The first
     */
    @JsonProperty("first")
    public Boolean getFirst() {
        return first;
    }

    /**
     * @param first The first
     */
    @JsonProperty("first")
    public void setFirst(Boolean first) {
        this.first = first;
    }

    /**
     * @return The size
     */
    @JsonProperty("size")
    public Integer getSize() {
        return size;
    }

    /**
     * @param size The size
     */
    @JsonProperty("size")
    public void setSize(Integer size) {
        this.size = size;
    }

    /**
     * @return The number
     */
    @JsonProperty("number")
    public Integer getNumber() {
        return number;
    }

    /**
     * @param number The number
     */
    @JsonProperty("number")
    public void setNumber(Integer number) {
        this.number = number;
    }
}