package com.hubplay.network.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hubplay.network.model.ErrorModel;


public class User extends ErrorModel {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("userName")
    private String userName;
    @JsonProperty("fullName")
    private String fullName;
    @JsonProperty("resetPassword")
    private String resetPassword;
    @JsonProperty("salt")
    private String salt;
    @JsonProperty("pwd")
    private String pwd;
    @JsonProperty("email")
    private String email;
    @JsonProperty("clientId")
    private Integer clientId;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("pointBalance")
    private Integer pointBalance;
    @JsonProperty("role")
    private String role;
    @JsonProperty("admin")
    private Boolean admin;

    /**
     * @return The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The userName
     */
    @JsonProperty("userName")
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName The userName
     */
    @JsonProperty("userName")
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return The fullName
     */
    @JsonProperty("fullName")
    public String getFullName() {
        return fullName;
    }

    /**
     * @param fullName The fullName
     */
    @JsonProperty("fullName")
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    /**
     * @return The resetPassword
     */
    @JsonProperty("resetPassword")
    public String getResetPassword() {
        return resetPassword;
    }

    /**
     * @param resetPassword The resetPassword
     */
    @JsonProperty("resetPassword")
    public void setResetPassword(String resetPassword) {
        this.resetPassword = resetPassword;
    }

    /**
     * @return The salt
     */
    @JsonProperty("salt")
    public String getSalt() {
        return salt;
    }

    /**
     * @param salt The salt
     */
    @JsonProperty("salt")
    public void setSalt(String salt) {
        this.salt = salt;
    }

    /**
     * @return The pwd
     */
    @JsonProperty("pwd")
    public String getPwd() {
        return pwd;
    }

    /**
     * @param pwd The pwd
     */
    @JsonProperty("pwd")
    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    /**
     * @return The email
     */
    @JsonProperty("email")
    public String getEmail() {
        return email;
    }

    /**
     * @param email The email
     */
    @JsonProperty("email")
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return The clientId
     */
    @JsonProperty("clientId")
    public Integer getClientId() {
        return clientId;
    }

    /**
     * @param clientId The clientId
     */
    @JsonProperty("clientId")
    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    /**
     * @return The icon
     */
    @JsonProperty("icon")
    public String getIcon() {
        return icon;
    }

    /**
     * @param icon The icon
     */
    @JsonProperty("icon")
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * @return The pointBalance
     */
    @JsonProperty("pointBalance")
    public Integer getPointBalance() {
        return pointBalance;
    }

    /**
     * @param pointBalance The pointBalance
     */
    @JsonProperty("pointBalance")
    public void setPointBalance(Integer pointBalance) {
        this.pointBalance = pointBalance;
    }

    /**
     * @return The role
     */
    @JsonProperty("role")
    public String getRole() {
        return role;
    }

    /**
     * @param role The role
     */
    @JsonProperty("role")
    public void setRole(String role) {
        this.role = role;
    }

    /**
     * @return The admin
     */
    @JsonProperty("admin")
    public Boolean getAdmin() {
        return admin;
    }

    /**
     * @param admin The admin
     */
    @JsonProperty("admin")
    public void setAdmin(Boolean admin) {
        this.admin = admin;
    }

}