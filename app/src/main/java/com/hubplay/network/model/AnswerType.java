package com.hubplay.network.model;

/**
 * 20.06.16.
 *
 * @author Alexey Vereshchaga
 */
public enum AnswerType {
    TEXT, ICON, LETTER
}
