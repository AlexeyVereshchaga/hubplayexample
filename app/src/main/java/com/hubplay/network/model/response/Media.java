
package com.hubplay.network.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hubplay.network.model.BaseModel;

import java.util.ArrayList;
import java.util.List;

public class Media extends BaseModel {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("fileName")
    private String fileName;
    @JsonProperty("fileSize")
    private Long fileSize;
    @JsonProperty("fileType")
    private String fileType;
    @JsonProperty("fileProperties")
    private List<Object> fileProperties = new ArrayList<>();
    @JsonProperty("url")
    private String url;
    @JsonProperty("mediaType")
    private String mediaType;
    @JsonProperty("play_duration")
    private Integer duration;

    /**
     * @return The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The fileName
     */
    @JsonProperty("fileName")
    public String getFileName() {
        return fileName;
    }

    /**
     * @param fileName The fileName
     */
    @JsonProperty("fileName")
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * @return The fileSize
     */
    @JsonProperty("fileSize")
    public Long getFileSize() {
        return fileSize;
    }

    /**
     * @param fileSize The fileSize
     */
    @JsonProperty("fileSize")
    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    /**
     * @return The fileType
     */
    @JsonProperty("fileType")
    public String getFileType() {
        return fileType;
    }

    /**
     * @param fileType The fileType
     */
    @JsonProperty("fileType")
    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    /**
     * @return The fileProperties
     */
    @JsonProperty("fileProperties")
    public List<Object> getFileProperties() {
        return fileProperties;
    }

    /**
     * @param fileProperties The fileProperties
     */
    @JsonProperty("fileProperties")
    public void setFileProperties(List<Object> fileProperties) {
        this.fileProperties = fileProperties;
    }

    /**
     * @return The url
     */
    @JsonProperty("url")
    public String getUrl() {
        return url;
    }

    /**
     * @param url The url
     */
    @JsonProperty("url")
    public void setUrl(String url) {
        this.url = url;
    }

    @JsonProperty("mediaType")
    public String getMediaType() {
        return mediaType;
    }

    @JsonProperty("mediaType")
    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    @JsonProperty("play_duration")
    public Integer getDuration() {
        return duration;
    }

    @JsonProperty("play_duration")
    public void setDuration(Integer duration) {
        this.duration = duration;
    }
}
