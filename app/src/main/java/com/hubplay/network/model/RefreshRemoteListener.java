package com.hubplay.network.model;

import android.support.v4.app.FragmentActivity;
import android.text.format.DateUtils;

import com.hubplay.controller.DataManager;
import com.hubplay.controller.TransitionsManager;
import com.hubplay.network.BaseHandler;
import com.hubplay.network.BaseRemoteListener;
import com.hubplay.network.model.response.Access;
import com.hubplay.utils.SharedPreferenceHelper;

import java.util.Date;

import retrofit2.Call;

/**
 * 18.04.16.
 *
 * @author Alexey Vereshchaga
 */
public class RefreshRemoteListener extends BaseRemoteListener<Access, ErrorModel> {

    public RefreshRemoteListener(FragmentActivity activity) {
        super(activity);
    }

    @Override
    public void onStartTask() {
        DataManager.isRefreshed = true;
    }

    @Override
    public void onSuccess(Access result) {
        if (result != null) {
            SharedPreferenceHelper.saveExpiredAt(new Date().getTime() + result.getExpiresIn() * DateUtils.SECOND_IN_MILLIS);
            SharedPreferenceHelper.saveAccessToken(result.getAccessToken());
            SharedPreferenceHelper.saveRefreshToken(result.getRefreshToken());
            SharedPreferenceHelper.saveTokenType(result.getTokenType());
            DataManager.isRefreshed = false;
            DataManager.repeatRequests();
        }
    }

    @Override
    public Boolean onFailure(Integer failureCode, ErrorModel error, Call<Access> call, BaseHandler<Access, ErrorModel> handler) {
        if (activity != null) {
            TransitionsManager.logout(activity);
        }
        return false;
    }

    @Override
    public void onError(Throwable throwable) {
        super.onError(throwable);
        DataManager.getCallsUnauthorized().clear();
        DataManager.isRefreshed = false;
    }
}
