package com.hubplay.network.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hubplay.network.model.BaseModel;

public class Sponsor extends BaseModel {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("title")
    private String name;
    @JsonProperty("icon")
    private String icon;

    /**
     * @return The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The icon
     */
    @JsonProperty("icon")
    public String getIcon() {
        return icon;
    }

    /**
     * @param icon The icon
     */
    @JsonProperty("icon")
    public void setIcon(String icon) {
        this.icon = icon;
    }
}
