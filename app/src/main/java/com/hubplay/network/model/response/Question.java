
package com.hubplay.network.model.response;

import android.support.annotation.NonNull;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hubplay.network.model.BaseModel;

import java.util.ArrayList;
import java.util.List;

public class Question extends BaseModel implements Comparable<Question> {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("text")
    private String text;
    @JsonProperty("title")
    private String title;
    @JsonProperty("media")
    private Media media;
    @JsonProperty("winMedia")
    private Media winMedia;
    @JsonProperty("loseMedia")
    private Media loseMedia;
    @JsonProperty("answers")
    private List<Answer> answers = new ArrayList<>();
    @JsonProperty("sponsor")
    private Sponsor sponsor;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("timer")
    private Integer timer;
    @JsonProperty("order")
    private Integer order;
    @JsonProperty("maxPoint")
    private Integer maxPoint;
    @JsonProperty("minPoint")
    private Integer minPoint;
    @JsonProperty("losePoint")
    private Integer losePoint;
    @JsonProperty("answerType")
    private String answerType;

    /**
     * @return The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The text
     */
    @JsonProperty("text")
    public String getText() {
        return text;
    }

    /**
     * @param text The text
     */
    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The media
     */
    @JsonProperty("media")
    public Media getMedia() {
        return media;
    }

    /**
     * @param media The media
     */
    @JsonProperty("media")
    public void setMedia(Media media) {
        this.media = media;
    }

    public Media getWinMedia() {
        return winMedia;
    }

    public void setWinMedia(Media winMedia) {
        this.winMedia = winMedia;
    }

    public Media getLoseMedia() {
        return loseMedia;
    }

    public void setLoseMedia(Media loseMedia) {
        this.loseMedia = loseMedia;
    }

    /**
     * @return The answers
     */
    @JsonProperty("answers")
    public List<Answer> getAnswers() {
        return answers;
    }

    /**
     * @param answers The answers
     */
    @JsonProperty("answers")
    public void setAnswers(List<Answer> answers) {
        this.answers = answers;
    }

    public Sponsor getSponsor() {
        return sponsor;
    }

    public void setSponsor(Sponsor sponsor) {
        this.sponsor = sponsor;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getTimer() {
        return timer;
    }

    public void setTimer(Integer timer) {
        this.timer = timer;
    }

    /**
     * @return The order
     */
    @JsonProperty("order")
    public Integer getOrder() {
        return order;
    }

    /**
     * @param order The order
     */
    @JsonProperty("order")
    public void setOrder(Integer order) {
        this.order = order;
    }

    public Integer getMaxPoint() {
        return maxPoint;
    }

    public void setMaxPoint(Integer maxPoint) {
        this.maxPoint = maxPoint;
    }

    public Integer getMinPoint() {
        return minPoint;
    }

    public void setMinPoint(Integer minPoint) {
        this.minPoint = minPoint;
    }

    public Integer getLosePoint() {
        return losePoint;
    }

    public void setLosePoint(Integer losePoint) {
        this.losePoint = losePoint;
    }

    public String getAnswerType() {
        return answerType;
    }

    public void setAnswerType(String answerType) {
        this.answerType = answerType;
    }

    @Override
    public int compareTo(@NonNull Question another) {
        int value = compare(another);
        return value == 0 ? getId().compareTo(another.getId()) : value;
    }

    private int compare(@NonNull Question another) {
        if (getOrder() == null || another.getOrder() == null) {
            return 0;
        }
        return getOrder().compareTo(another.getOrder());
    }
}
