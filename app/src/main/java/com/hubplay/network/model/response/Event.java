
package com.hubplay.network.model.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hubplay.network.model.BaseModel;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class Event extends BaseModel {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("termType")
    private String termType;
    @JsonProperty("paymentType")
    private String paymentType;
    @JsonProperty("category")
    private String category;
    @JsonProperty("difficulty")
    private String difficulty;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "s")
    @JsonProperty("startDate")
    private Date startDate;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "s")
    @JsonProperty("endDate")
    private Date endDate;
    @JsonProperty("league")
    private League league;
    @JsonProperty("user")
    private User user;
    @JsonProperty("preMedia")
    private Media preMedia;
    @JsonProperty("postMedia")
    private Media postMedia;
    @JsonProperty("question")
    private List<Question> question = new ArrayList<>();
    @JsonProperty("teams")
    private List<Team> teams = new ArrayList<>();
    @JsonProperty("hostedBy")
    private User hostedBy;
    @JsonProperty("title")
    private String title;
    @JsonProperty("subtitle")
    private String subtitle;
    @JsonProperty("description")
    private String description;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("bgIcon")
    private String bgIcon;
    @JsonProperty("watches")
    private List<Watch> watches = new ArrayList<>();

    /**
     * @return The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The termType
     */
    @JsonProperty("termType")
    public String getTermType() {
        return termType;
    }

    /**
     * @param termType The termType
     */
    @JsonProperty("termType")
    public void setTermType(String termType) {
        this.termType = termType;
    }

    /**
     * @return The paymentType
     */
    @JsonProperty("paymentType")
    public String getPaymentType() {
        return paymentType;
    }

    /**
     * @param paymentType The paymentType
     */
    @JsonProperty("paymentType")
    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    /**
     * @return The category
     */
    @JsonProperty("category")
    public String getCategory() {
        return category;
    }

    /**
     * @param category The difficulty
     */
    @JsonProperty("category")
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return The difficulty
     */
    @JsonProperty("difficulty")
    public String getDifficulty() {
        return difficulty;
    }

    /**
     * @param difficulty The difficulty
     */
    @JsonProperty("difficulty")
    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }

    /**
     * @return The startDate
     */
    @JsonProperty("startDate")
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate The startDate
     */
    @JsonProperty("startDate")
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return The endDate
     */
    @JsonProperty("endDate")
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate The endDate
     */
    @JsonProperty("endDate")
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * @return The league
     */
    @JsonProperty("league")
    public League getLeague() {
        return league;
    }

    /**
     * @param league The league
     */
    @JsonProperty("league")
    public void setLeague(League league) {
        this.league = league;
    }

    /**
     * @return The user
     */
    @JsonProperty("user")
    public User getUser() {
        return user;
    }

    /**
     * @param user The user
     */
    @JsonProperty("user")
    public void setUser(User user) {
        this.user = user;
    }


    public Media getPreMedia() {
        return preMedia;
    }

    public void setPreMedia(Media preMedia) {
        this.preMedia = preMedia;
    }

    public Media getPostMedia() {
        return postMedia;
    }

    public void setPostMedia(Media postMedia) {
        this.postMedia = postMedia;
    }

    /**
     * @return The question
     */
    @JsonProperty("question")
    public List<Question> getQuestion() {
        Collections.sort(question);
        return question;
    }

    /**
     * @param question The question
     */
    @JsonProperty("question")
    public void setQuestion(List<Question> question) {
        this.question = question;
    }

    /**
     * @return The teams
     */
    @JsonProperty("teams")
    public List<Team> getTeams() {
        return teams;
    }

    /**
     * @param teams The teams
     */
    @JsonProperty("teams")
    public void setTeams(List<Team> teams) {
        this.teams = teams;
    }

    public User getHostedBy() {
        return hostedBy;
    }

    public void setHostedBy(User hostedBy) {
        this.hostedBy = hostedBy;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getBgIcon() {
        return bgIcon;
    }

    public void setBgIcon(String bgIcon) {
        this.bgIcon = bgIcon;
    }

    public List<Watch> getWatches() {
        return watches;
    }

    public void setWatches(List<Watch> watches) {
        this.watches = watches;
    }
}
