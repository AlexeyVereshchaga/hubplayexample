package com.hubplay.network.model;

/**
 * 01.06.16.
 *
 * @author Alexey Vereshchaga
 */
public enum GrantType {
    PASSWORD,
    REFRESH_TOKEN
}
