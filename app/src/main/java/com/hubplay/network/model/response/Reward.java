package com.hubplay.network.model.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.hubplay.network.model.ErrorModel;

import java.util.Date;

public class Reward extends ErrorModel {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("description")
    private String description;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "s")
    @JsonProperty("expirationDate")
    private Date expirationDate;
    @JsonProperty("value")
    private String value;
    @JsonProperty("price")
    private Integer price;
    @JsonProperty("qrCode")
    private String qrCode;
    @JsonProperty("qrUrl")
    private String qrUrl;

    /**
     * @return The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The title
     */
    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The icon
     */
    @JsonProperty("icon")
    public String getIcon() {
        return icon;
    }

    /**
     * @param icon The icon
     */
    @JsonProperty("icon")
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * @return The description
     */
    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The expirationDate
     */
    @JsonProperty("expirationDate")
    public Date getExpirationDate() {
        return expirationDate;
    }

    /**
     * @param expirationDate The expirationDate
     */
    @JsonProperty("expirationDate")
    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    /**
     * @return The value
     */
    @JsonProperty("value")
    public String getValue() {
        return value;
    }

    /**
     * @param value The value
     */
    @JsonProperty("value")
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * @return The price
     */
    @JsonProperty("price")
    public Integer getPrice() {
        return price;
    }

    /**
     * @param price The price
     */
    @JsonProperty("price")
    public void setPrice(Integer price) {
        this.price = price;
    }

    /**
     * @return The qrCode
     */
    @JsonProperty("qrCode")
    public String getQrCode() {
        return qrCode;
    }

    /**
     * @param qrCode The qrCode
     */
    @JsonProperty("qrCode")
    public void setQrCode(String qrCode) {
        this.qrCode = qrCode;
    }

    /**
     * @return The qrUrl
     */
    @JsonProperty("qrUrl")
    public String getQrUrl() {
        return qrUrl;
    }

    /**
     * @param qrUrl The qrUrl
     */
    @JsonProperty("qrUrl")
    public void setQrUrl(String qrUrl) {
        this.qrUrl = qrUrl;
    }

}
