package com.hubplay.network.model;

/**
 * 10.06.16.
 *
 * @author Alexey Vereshchaga
 */
public enum MediaType {
    MP4, IMAGE, GIF, YOUTUBE, NONE
}
