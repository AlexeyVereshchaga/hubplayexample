package com.hubplay.network.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hubplay.network.model.BaseModel;

public class Sort extends BaseModel {

    @JsonProperty("direction")
    private String direction;
    @JsonProperty("property")
    private String property;
    @JsonProperty("ignoreCase")
    private Boolean ignoreCase;
    @JsonProperty("nullHandling")
    private String nullHandling;
    @JsonProperty("ascending")
    private Boolean ascending;

    /**
     * @return The direction
     */
    @JsonProperty("direction")
    public String getDirection() {
        return direction;
    }

    /**
     * @param direction The direction
     */
    @JsonProperty("direction")
    public void setDirection(String direction) {
        this.direction = direction;
    }

    /**
     * @return The property
     */
    @JsonProperty("property")
    public String getProperty() {
        return property;
    }

    /**
     * @param property The property
     */
    @JsonProperty("property")
    public void setProperty(String property) {
        this.property = property;
    }

    /**
     * @return The ignoreCase
     */
    @JsonProperty("ignoreCase")
    public Boolean getIgnoreCase() {
        return ignoreCase;
    }

    /**
     * @param ignoreCase The ignoreCase
     */
    @JsonProperty("ignoreCase")
    public void setIgnoreCase(Boolean ignoreCase) {
        this.ignoreCase = ignoreCase;
    }

    /**
     * @return The nullHandling
     */
    @JsonProperty("nullHandling")
    public String getNullHandling() {
        return nullHandling;
    }

    /**
     * @param nullHandling The nullHandling
     */
    @JsonProperty("nullHandling")
    public void setNullHandling(String nullHandling) {
        this.nullHandling = nullHandling;
    }

    /**
     * @return The ascending
     */
    @JsonProperty("ascending")
    public Boolean getAscending() {
        return ascending;
    }

    /**
     * @param ascending The ascending
     */
    @JsonProperty("ascending")
    public void setAscending(Boolean ascending) {
        this.ascending = ascending;
    }
}