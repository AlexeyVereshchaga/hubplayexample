
package com.hubplay.network.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hubplay.network.model.BaseModel;

public class Team extends BaseModel {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("title")
    private String title;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("leagueId")
    private Integer leagueId;

    /**
     * @return The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The title
     */
    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The icon
     */
    @JsonProperty("icon")
    public String getIcon() {
        return icon;
    }

    /**
     * @param icon The icon
     */
    @JsonProperty("icon")
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * @return The leagueId
     */
    @JsonProperty("leagueId")
    public Integer getLeagueId() {
        return leagueId;
    }

    /**
     * @param leagueId The leagueId
     */
    @JsonProperty("leagueId")
    public void setLeagueId(Integer leagueId) {
        this.leagueId = leagueId;
    }
}
