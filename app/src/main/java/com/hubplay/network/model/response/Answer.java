
package com.hubplay.network.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hubplay.network.model.BaseModel;


public class Answer extends BaseModel {

    @JsonProperty("id")
    private Integer id;
    @JsonProperty("text")
    private String text;
    @JsonProperty("correct")
    private Boolean correct;
    @JsonProperty("icon")
    private String icon;

    /**
     * @return The id
     */
    @JsonProperty("id")
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    @JsonProperty("id")
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The text
     */
    @JsonProperty("text")
    public String getText() {
        return text;
    }

    /**
     * @param text The text
     */
    @JsonProperty("text")
    public void setText(String text) {
        this.text = text;
    }

    /**
     * @return The correct
     */
    @JsonProperty("correct")
    public Boolean getCorrect() {
        return correct;
    }

    /**
     * @param correct The correct
     */
    @JsonProperty("correct")
    public void setCorrect(Boolean correct) {
        this.correct = correct;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
