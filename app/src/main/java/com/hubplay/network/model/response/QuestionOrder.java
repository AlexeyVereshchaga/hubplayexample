package com.hubplay.network.model.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.hubplay.network.model.BaseModel;

/**
 * 19.07.16.
 *
 * @author Alexey Vereshchaga
 */
public class QuestionOrder extends BaseModel {
    @JsonProperty("error")
    private String error;
    @JsonProperty("nextQuestionOrder")
    private Integer nextQuestionOrder;
    @JsonProperty("points")
    private Integer points;

    /**
     * @return The error
     */
    @JsonProperty("error")
    public String getError() {
        return error;
    }

    /**
     * @param error The error
     */
    @JsonProperty("error")
    public void setError(String error) {
        this.error = error;
    }

    /**
     * @return The nextQuestionOrder
     */
    @JsonProperty("nextQuestionOrder")
    public Integer getNextQuestionOrder() {
        return nextQuestionOrder;
    }

    /**
     * @param nextQuestionOrder The nextQuestionOrder
     */
    @JsonProperty("nextQuestionOrder")
    public void setNextQuestionOrder(Integer nextQuestionOrder) {
        this.nextQuestionOrder = nextQuestionOrder;
    }

    /**
     * @return The points
     */
    @JsonProperty("points")
    public Integer getPoints() {
        return points;
    }

    /**
     * @param points The points
     */
    @JsonProperty("points")
    public void setPoints(Integer points) {
        this.points = points;
    }

}
