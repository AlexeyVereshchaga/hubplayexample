package com.hubplay.network.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * 15.04.16.
 *
 * @author Alexey Vereshchaga
 */
public class ErrorModel extends BaseModel {
    @JsonProperty("error")
    private String error;
    @JsonProperty("error_description")
    private String errorDescription;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getErrorDescription() {
        return errorDescription;
    }

    public void setErrorDescription(String errorDescription) {
        this.errorDescription = errorDescription;
    }
}
