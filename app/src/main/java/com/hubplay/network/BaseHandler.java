package com.hubplay.network;

import android.util.Log;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BaseHandler<T, ERROR> implements Callback<T> {

    private static final String TAG = BaseHandler.class.getSimpleName();

    private IRemoteListener<T, ERROR> remoteListener;
    private final TypeReference<ERROR> errorTypeReference;

    public BaseHandler(IRemoteListener<T, ERROR> remoteListener, TypeReference<ERROR> errorTypeReference) {
        this.remoteListener = remoteListener;
        this.errorTypeReference = errorTypeReference;
    }

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response != null) {
            if (response.isSuccessful()) {
                remoteListener.onSuccess(response.body());
            } else {
                Integer errorCode = response.code();
                ERROR errorObject = null;
                if (response.errorBody() != null) {
                    try {
                        String errorString = response.errorBody().string();
                        errorObject = new ObjectMapper().readValue(errorString, errorTypeReference);
                    } catch (Exception e) {
                        Log.e(TAG, "Error", e);
                    }
                    remoteListener.onFailure(errorObject);
                }
                remoteListener.onFailure(errorCode, errorObject, call, this);
                remoteListener.onError(null);
            }
        }
        remoteListener.onFinishTask();
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        remoteListener.onError(t);
        remoteListener.onFinishTask();
    }

    public void onStart() {
        remoteListener.onStartTask();
    }
}

