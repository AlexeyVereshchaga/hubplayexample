package com.hubplay.utils;

/**
 * 21.04.16.
 *
 * @author Alexey Vereshchaga
 */
public class Constant {
    public static final String TAG = "hubplay_tag";

    public static final String SERVER_KEY = "server";
    public static final String ACCESS_TOKEN_KEY = "access_token_key";
    public static final String REFRESH_TOKEN_KEY = "refresh_token_key";
    public static final String TOKEN_TYPE_KEY = "token_type_key";
    public static final String EXPIRE_AT_KEY = "expire_at";

    public static final String OFFLINE_DIALOG_KEY = "offline_dialog_key";
    public static final String OFFLINE_DIALOG_TAG = "offline_dialog_tag";
    public static final String USER = "user_key";
    public static final String EVENT_DIALOG_KEY = "event_dialog_key";

    public static final String EVENT_KEY = "event_key";
}
