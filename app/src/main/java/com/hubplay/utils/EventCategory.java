package com.hubplay.utils;

/**
 * 15.07.16.
 *
 * @author Alexey Vereshchaga
 */
public enum EventCategory {
    FEATURED, SPORTS
}
