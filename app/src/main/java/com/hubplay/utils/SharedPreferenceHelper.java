package com.hubplay.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;

import com.hubplay.TheApplication;
import com.hubplay.network.Server;

import java.util.HashSet;
import java.util.Set;

/**
 * 21.04.16.
 *
 * @author Alexey Vereshchaga
 */
public class SharedPreferenceHelper {

    private static SharedPreferences getPreferences() {
        return PreferenceManager.getDefaultSharedPreferences(TheApplication.getInstance().getApplicationContext());
    }

    @NonNull
    public static String getServer() {
        return getPreferences().getString(Constant.SERVER_KEY, Server.DEV.getUrl());
    }

    @NonNull
    public static String getAccessToken() {
        return getPreferences().getString(Constant.ACCESS_TOKEN_KEY, "");
    }

    @NonNull
    public static String getRefreshToken() {
        return getPreferences().getString(Constant.REFRESH_TOKEN_KEY, "");
    }

    @NonNull
    public static String getTokenType() {
        return getPreferences().getString(Constant.TOKEN_TYPE_KEY, "");
    }

    public static Long getExpiredAt() {
        return getPreferences().getLong(Constant.EXPIRE_AT_KEY, 0);
    }


    public static void saveServer(Server server) {
        getPreferences().edit().putString(Constant.SERVER_KEY, server.getUrl()).apply();
    }

    public static void saveAccessToken(String accessToken) {
        getPreferences().edit().putString(Constant.ACCESS_TOKEN_KEY, accessToken).apply();
    }

    public static void saveRefreshToken(String accessToken) {
        getPreferences().edit().putString(Constant.REFRESH_TOKEN_KEY, accessToken).apply();
    }

    public static void saveTokenType(String tokenType) {
        getPreferences().edit().putString(Constant.TOKEN_TYPE_KEY, tokenType).apply();
    }

    public static void saveExpiredAt(Long expiredAt) {
        getPreferences().edit().putLong(Constant.EXPIRE_AT_KEY, expiredAt).apply();
    }

    public static void deleteServer() {
        getPreferences().edit().remove(Constant.SERVER_KEY).commit();
    }

    public static void deleteAccessToken() {
        getPreferences().edit().remove(Constant.ACCESS_TOKEN_KEY).commit();
    }

    public static void deleteRefreshToken() {
        getPreferences().edit().remove(Constant.REFRESH_TOKEN_KEY).commit();
    }

    public static void deleteTokenType() {
        getPreferences().edit().remove(Constant.TOKEN_TYPE_KEY).commit();
    }

    public static void deleteExpiredAt() {
        getPreferences().edit().remove(Constant.EXPIRE_AT_KEY).commit();
    }

    public static void markOfflineDialogShown(Activity activity) {
        Set<String> stringSet = PreferenceManager.getDefaultSharedPreferences(activity).getStringSet(Constant.OFFLINE_DIALOG_KEY, null);
        if (stringSet == null) {
            stringSet = new HashSet<>();
        }
        stringSet.add(activity.getClass().getSimpleName());
        PreferenceManager.getDefaultSharedPreferences(activity).edit()
                .putStringSet(Constant.OFFLINE_DIALOG_KEY, stringSet).apply();
    }

    public static void markOfflineDialogNotShown(Activity activity) {
        Set<String> stringSet = PreferenceManager.getDefaultSharedPreferences(activity).getStringSet(Constant.OFFLINE_DIALOG_KEY, null);
        if (stringSet != null) {
            stringSet.remove(activity.getClass().getSimpleName());
            PreferenceManager.getDefaultSharedPreferences(activity).edit()
                    .putStringSet(Constant.OFFLINE_DIALOG_KEY, stringSet).apply();
        }
    }

    public static boolean isOfflineDialogShow(Activity activity) {
        Set<String> stringSet = PreferenceManager.getDefaultSharedPreferences(activity).getStringSet(Constant.OFFLINE_DIALOG_KEY, null);
        if (stringSet != null) {
            return stringSet.contains(activity.getClass().getSimpleName());
        }
        return false;
    }

    public static void markEventDialogShown() {
        getPreferences().edit()
                .putBoolean(Constant.EVENT_DIALOG_KEY, true).apply();
    }

    public static void markEventDialogNotShown() {
        getPreferences().edit()
                .putBoolean(Constant.EVENT_DIALOG_KEY, false).apply();
    }

    public static boolean isEventDialogShow() {
        return getPreferences().getBoolean(Constant.EVENT_DIALOG_KEY, false);
    }
}
