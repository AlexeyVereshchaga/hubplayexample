package com.hubplay.utils.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * 16.06.16.
 *
 * @author Alexey Vereshchaga
 */
public class SquareTextView extends TextView {
    private float heightRatio;
    private int maxHeight;

    public SquareTextView(Context context) {
        super(context);
    }

    public SquareTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        heightRatio = attrs.getAttributeFloatValue(null, "heightRatio", 1.0f);
        maxHeight = -1;
    }

    public SquareTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        int height = bottom - top;
        ViewGroup.LayoutParams params = getLayoutParams();
        params.width = height;
        params.height = height;
//        this.setLayoutParams(params);
//        this.setMeasuredDimension(height, height);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        //todo gravity will be fixed after research the sequence of content rendering
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int size;
        int width = getMeasuredWidth();
        int height = getMeasuredHeight();
        size = width < height ? height : width;
        setMeasuredDimension(size, size);
    }

}
