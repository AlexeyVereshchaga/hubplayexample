package com.hubplay.utils;

import android.content.Context;
import android.graphics.Point;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ListView;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hubplay.R;
import com.hubplay.network.model.response.Question;
import com.hubplay.view.fragment.dialog.OfflineDialog;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * 30.05.16.
 *
 * @author Alexey Vereshchaga
 */
public class HelpUtils {

    private final static String TAG = HelpUtils.class.getSimpleName();

    public static int validateFields(String firstLastName, String username, String email, String password, String repeatPassword) {
        if (TextUtils.isEmpty(firstLastName)) {
            return R.string.sign_up_empty_full_name;
        }
        if (TextUtils.isEmpty(email)) {
            return R.string.sign_up_empty_email;
        }
        if (TextUtils.isEmpty(username)) {
            return R.string.sign_up_empty_username;
        }
        if (TextUtils.isEmpty(password)) {
            return R.string.sign_up_empty_password;
        }
        //// TODO: 06.07.16 need message
        if (TextUtils.isEmpty(repeatPassword)) {
            return R.string.sign_up_empty_password;
        }
        if (!isValidEmail(email)) {
            return R.string.sign_up_invalid_email;
        }
        if (!isValidPassword(password)) {
            return R.string.sign_up_invalid_password;
        }
        //// TODO: 06.07.16 need message
        if (!password.equals(repeatPassword)) {
            return R.string.registration_password_repeat_password_dont_match;
        }
        return -1;
    }

    public static int validateLoginFields(String username, String password) {
        if (TextUtils.isEmpty(username) && TextUtils.isEmpty(password)) {
            return R.string.sign_in_empty_username_and_password;
        }
        if (TextUtils.isEmpty(username)) {
            return R.string.sign_up_empty_username;
        }
        if (TextUtils.isEmpty(password)) {
            return R.string.sign_in_empty_password;
        }
        if (!isValidPassword(password)) {
            return R.string.sign_up_invalid_password;
        }
        return -1;
    }

    public static int validUsernameField(String username) {
        if (TextUtils.isEmpty(username)) {
            return R.string.sign_up_empty_username;
        }
        return -1;
    }

    public static int validateEmail(String email) {
        if (TextUtils.isEmpty(email)) {
            return R.string.sign_up_empty_email;
        }
        if (!isValidEmail(email)) {
            return R.string.sign_up_invalid_email;
        }
        return -1;
    }

    private static boolean isValidPassword(String password) {
        Pattern p = Pattern.compile(".{3,}");
        return p.matcher(password).matches();
    }

    public static boolean isValidEmail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    @Nullable
    public static String createStringFromObject(Object o) {
        ObjectMapper objectMapper = new ObjectMapper();
        String objectStr = null;
        try {
            objectStr = objectMapper.writeValueAsString(o);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return objectStr;
    }

    @Nullable
    public static <T> T readStringToObject(String eventStr, Class<T> aClass) {
        T object = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            object = objectMapper.readValue(eventStr, aClass);
        } catch (IOException e) {
            Log.e(TAG, "readStringToObject: ", e);
        }
        return object;
    }

    @Nullable
    public static <T> T readStringToObject(String eventStr, TypeReference<T> typeReference) {
        T object = null;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            object = objectMapper.readValue(eventStr, typeReference);
        } catch (IOException e) {
            Log.e(TAG, "readStringToObject: ", e);
        }
        return object;
    }

    public static void showOfflineDialog(FragmentActivity activity) {
        if (activity != null && !SharedPreferenceHelper.isOfflineDialogShow(activity)) {
            try {
                OfflineDialog dialog = new OfflineDialog();
                FragmentTransaction ft = activity.getSupportFragmentManager().beginTransaction();
                ft.add(dialog, Constant.OFFLINE_DIALOG_TAG);
                ft.commitAllowingStateLoss();
                SharedPreferenceHelper.markOfflineDialogShown(activity);
            } catch (Exception e) {
                Log.e(TAG, "showOfflineDialog: ", e);
            }
        }
    }

    public static int getPoints(int maxPoints, int minPoints, int losePoints, int timeInSeconds, boolean correct) {
        int points;
        if (correct) {
            int varyingPoints = maxPoints - minPoints;
            if (timeInSeconds < 1) {
                points = maxPoints;
            } else if (timeInSeconds < 3) {
                points = minPoints + (int) Math.round(0.85 * varyingPoints);
            } else if (timeInSeconds < 5) {
                points = minPoints + (int) Math.round(0.71 * varyingPoints);
            } else if (timeInSeconds < 7) {
                points = minPoints + (int) Math.round(0.42 * varyingPoints);
            } else {
                points = minPoints;
            }
        } else {
            points = losePoints;
        }
        return points;
    }

    public static int getNumberOfDays(Date date) {
        return date != null ? getDiffDays(date, new Date()) : -1;
    }

    private static int getDiffDays(Date first, Date last) {
        long diff = first.getTime() - last.getTime();
        return (int) TimeUnit.MILLISECONDS.toDays(diff);
    }

    public static void deleteTokens() {
        SharedPreferenceHelper.deleteAccessToken();
        SharedPreferenceHelper.deleteRefreshToken();
        SharedPreferenceHelper.deleteTokenType();
        SharedPreferenceHelper.deleteExpiredAt();
    }

    public static int getEventPoints(List<Question> questions) {
        int points = 0;
        if (questions != null) {
            for (Question question :
                    questions) {
                points += question.getMaxPoint();
            }
        }
        return points;
    }

    @Nullable
    public static <T extends Enum<T>> T getEnumByName(Class<T> enumClass, String mediaTypeStr) {
        T mediaType = null;
        try {
            mediaType = T.valueOf(enumClass, mediaTypeStr);
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "", e);
        }
        return mediaType;
    }

    public static int getDisplayHeight(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        return size.y;
    }

    public static View getViewByPosition(int pos, ListView listView) {
        final int firstListItemPosition = listView.getFirstVisiblePosition();
        final int lastListItemPosition = firstListItemPosition + listView.getChildCount() - 1;

        if (pos < firstListItemPosition || pos > lastListItemPosition) {
            return listView.getAdapter().getView(pos, null, listView);
        } else {
            final int childIndex = pos - firstListItemPosition;
            return listView.getChildAt(childIndex);
        }
    }

    @NonNull
    public static Point getLocationOfView(View view) {
        int[] location = new int[2];
        // Get the x, y location and store it in the location[] array
        view.getLocationOnScreen(location);
        //Initialize the Point with x, and y positions
        Point point = new Point();
        point.x = location[0];
        point.y = location[1];
        return point;
    }
}
