package com.hubplay.utils;

/**
 * 09.06.16.
 *
 * @author Alexey Vereshchaga
 */
public enum ContestPaymentType {
    FREE, PAID
}
