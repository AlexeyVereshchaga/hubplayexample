package com.hubplay.utils;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;

/**
 * @author Alexey Vereshchaga
 */
public class BubbleDrawable extends Drawable {

    // Public Class Constants
    ////////////////////////////////////////////////////////////

    public static final int OFFSET = 10;
    public static final int POINTER_WIDTH = 30;
    public static final int POINTER_HEIGHT = 30;
    public static final int STROKE_WIDTH = 2;

    // Private Instance Variables
    ////////////////////////////////////////////////////////////

    private Paint mPaintRect;
    private Paint mPaintBorder;
    private Paint mPaintPointer;
    private int mBoxColor;
    private int mBorderColor;
    private int mPointerColor;

    private RectF mBoxRect;
    private int mBoxWidth;
    private int mBoxHeight;
    private Rect mBoxPadding = new Rect();

    private Path mPointer;
    private Path mPathBorder;

    private int mPointerWidth;
    private int mPointerHeight;
    private PointerAlignment mPointerAlignment;

    // Constructors
    ////////////////////////////////////////////////////////////

    public BubbleDrawable(PointerAlignment pointerAlignment, int boxColor, int pointerColor, int borderColor) {
        setPointerAlignment(pointerAlignment);
        initBubble(boxColor, pointerColor, borderColor);
    }

    // Setters
    ////////////////////////////////////////////////////////////

    public void setPadding(int left, int top, int right, int bottom) {
        mBoxPadding.left = left;
        mBoxPadding.top = top;
        mBoxPadding.right = right;
        mBoxPadding.bottom = bottom;
    }

    public void setPointerAlignment(PointerAlignment pointerAlignment) {
        mPointerAlignment = pointerAlignment;
    }

    public void setPointerWidth(int pointerWidth) {
        mPointerWidth = pointerWidth;
    }

    public void setPointerHeight(int pointerHeight) {
        mPointerHeight = pointerHeight;
    }

    private void initBubble(int boxColor, int pointerColor, int borderColor) {
        mPaintRect = new Paint();
        mPaintRect.setAntiAlias(true);
        mPaintPointer = new Paint();
        mPaintPointer.setAntiAlias(true);
        mPaintBorder = new Paint();
        mPaintBorder.setAntiAlias(true);

        mBoxColor = boxColor;
        mPointerColor = pointerColor;
        mBorderColor = borderColor;
        mPaintRect.setColor(mBoxColor);
        mPaintPointer.setColor(mPointerColor);
        mPaintBorder.setColor(mBorderColor);
        setPointerWidth(POINTER_WIDTH);
        setPointerHeight(POINTER_HEIGHT);
    }

    private void updatePointerPath() {
        mPointer = new Path();
        //Border
        mPathBorder = new Path();

        mPaintBorder.setAntiAlias(true);
        mPaintBorder.setStyle(Paint.Style.STROKE);
//        mPaint.setPathEffect(new CornerPathEffect(10));
        mPaintBorder.setStrokeWidth(STROKE_WIDTH);
        mPaintBorder.setAntiAlias(true);

        mPointer.setFillType(Path.FillType.EVEN_ODD);
        switch (mPointerAlignment) {
            case LEFT:
                // Set the starting point
                mPointer.moveTo(mPointerHeight, OFFSET);
                // Define the lines
                mPointer.rLineTo(-mPointerHeight, mPointerWidth / 2);
                mPointer.rLineTo(mPointerHeight, mPointerWidth / 2);
                mPointer.rLineTo(0, -mPointerWidth);

                //border without radius
                mPathBorder.moveTo(mPointerHeight, OFFSET);        //0
                mPathBorder.rLineTo(-mPointerHeight, mPointerWidth / 2);        //1
                mPathBorder.rLineTo(mPointerHeight, mPointerWidth / 2);         //2
                mPathBorder.rLineTo(0, mBoxHeight - mPointerWidth - OFFSET - STROKE_WIDTH / 2);    //3
                mPathBorder.rLineTo(mBoxWidth - POINTER_HEIGHT - STROKE_WIDTH / 2, 0);             //4
                mPathBorder.rLineTo(0, -mBoxHeight + STROKE_WIDTH);                                //5
                mPathBorder.rLineTo(-mBoxWidth + POINTER_HEIGHT + STROKE_WIDTH, 0);            //6
                mPathBorder.rLineTo(0, OFFSET - STROKE_WIDTH);                                 //7
                break;
            case RIGHT:
                mPointer.moveTo(mBoxWidth, OFFSET);
                // Define the lines
                mPointer.rLineTo(mPointerHeight, mPointerWidth / 2);
                mPointer.rLineTo(-mPointerHeight, mPointerWidth / 2);
                mPointer.rLineTo(0, -mPointerWidth);

                //border
                mPathBorder.moveTo(mBoxWidth, OFFSET);             //0
                mPathBorder.rLineTo(mPointerHeight, mPointerWidth / 2);         //1
                mPathBorder.rLineTo(-mPointerHeight, mPointerWidth / 2);        //2
                mPathBorder.rLineTo(0, mBoxHeight - mPointerWidth - OFFSET - STROKE_WIDTH / 2);    //3
                mPathBorder.rLineTo(-mBoxWidth + STROKE_WIDTH / 2, 0);                             //4
                mPathBorder.rLineTo(0, -mBoxHeight + STROKE_WIDTH);                            //5
                mPathBorder.rLineTo(mBoxWidth - STROKE_WIDTH, 0);                              //6
                mPathBorder.rLineTo(0, OFFSET - STROKE_WIDTH);                                 //7
                break;
            case BOTTOM:
                int radius = mPointerHeight;
                RectF oval = new RectF();
                float x = mPointerHeight;
                float y = mBoxHeight + mPointerHeight;
                oval.set(x - radius, y - radius, x + radius, y + radius);
                mPointer.moveTo(0, mBoxHeight);//1
                mPointer.rLineTo(0, mPointerHeight);//2
                mPointer.arcTo(oval, 180, 90);//3
                mPointer.lineTo(0, mBoxHeight);
                //border
                mPathBorder.moveTo(STROKE_WIDTH / 2, mBoxHeight + mPointerHeight);//0
                mPathBorder.arcTo(oval, 180, 90);
                mPathBorder.lineTo(mBoxWidth - STROKE_WIDTH / 2, mBoxHeight);
                mPathBorder.lineTo(mBoxWidth - STROKE_WIDTH / 2, STROKE_WIDTH / 2);
                mPathBorder.lineTo(STROKE_WIDTH / 2, STROKE_WIDTH / 2);
                break;
            case TOP:
                radius = mPointerHeight;
                oval = new RectF();
                x = mPointerHeight;
                y = 0;
                oval.set(x - radius, y - radius, x + radius, y + radius);
                mPointer.moveTo(0, 0);//1
                mPointer.rLineTo(0, mPointerHeight);//2
                mPointer.arcTo(oval, 90, 90);//3
                mPointer.lineTo(0, 0);
                //border
//                mPathBorder.moveTo(radius, radius);

                RectF ovalLine = new RectF();
                radius -= 1;
                ovalLine.set(x - radius, y - radius, x + radius, y + radius);
                mPathBorder.arcTo(ovalLine, 90, 90);
                mPathBorder.lineTo(STROKE_WIDTH / 2, mBoxHeight - STROKE_WIDTH / 2);
                mPathBorder.lineTo(mBoxWidth - STROKE_WIDTH / 2, mBoxHeight - STROKE_WIDTH / 2);
                mPathBorder.lineTo(mBoxWidth - STROKE_WIDTH / 2, mPointerHeight - STROKE_WIDTH / 2);
//                mPathBorder.lineTo(mBoxWidth - STROKE_WIDTH / 2,  mBoxHeight - STROKE_WIDTH);
                break;
        }
        mPointer.close();
        mPathBorder.close();
    }

    // Superclass Override Methods
    ////////////////////////////////////////////////////////////

    @Override
    public void draw(Canvas canvas) {
        switch (mPointerAlignment) {
            case LEFT:
                mBoxRect = new RectF(mPointerHeight, 0.0f, mBoxWidth, mBoxHeight);
                break;
            case RIGHT:
                mBoxRect = new RectF(0.0f, 0.0f, mBoxWidth, mBoxHeight);
                break;
            case BOTTOM:
                mBoxRect = new RectF(0.0f, 0.0f, mBoxWidth, mBoxHeight);
                break;
            case TOP:
                mBoxRect = new RectF(0.0f, mPointerHeight, mBoxWidth, mBoxHeight);
                break;
        }
        canvas.drawRect(mBoxRect, mPaintRect);
        updatePointerPath();
        canvas.drawPath(mPointer, mPaintPointer);
        // border
        canvas.drawPath(mPathBorder, mPaintBorder);
    }

    @Override
    public int getOpacity() {
        return 255;
    }

    @Override
    public void setAlpha(int alpha) {
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
    }

    @Override
    public boolean getPadding(@NonNull Rect padding) {
        padding.set(mBoxPadding);

        // Adjust the padding to include the height of the pointer
        switch (mPointerAlignment) {
            case LEFT:
                padding.left += mPointerHeight;
                break;
            case RIGHT:
                padding.right += mPointerHeight;
                break;
            case BOTTOM:
                padding.bottom += mPointerHeight;
                break;
            case TOP:
                padding.top += mPointerHeight;
                break;
        }
        return true;
    }

    @Override
    protected void onBoundsChange(Rect bounds) {
        switch (mPointerAlignment) {
            case LEFT:
                mBoxWidth = bounds.width();
                mBoxHeight = bounds.height();
                break;
            case RIGHT:
                mBoxWidth = bounds.width() - mPointerHeight;
                mBoxHeight = bounds.height();
                break;
            case BOTTOM:
                mBoxWidth = bounds.width();
                mBoxHeight = bounds.height() - mPointerHeight;
                break;
            case TOP:
                mBoxWidth = bounds.width();
                mBoxHeight = bounds.height();
                break;
        }
        super.onBoundsChange(bounds);
    }

    public enum PointerAlignment {
        LEFT, RIGHT, TOP, BOTTOM
    }
}
