package com.hubplay.utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.hubplay.view.fragment.create_contest.tab.LeagueContestFragment;
import com.hubplay.view.fragment.create_contest.tab.ShowContestFragment;

/**
 * 03.06.16.
 *
 * @author Alexey Vereshchaga
 */
public enum ContestType {
    SHOW(ShowContestFragment.class, "Shows"),
    GAME(LeagueContestFragment.class, "Games");


    private Class<? extends Fragment> fragmentClass;
    private String title;

    ContestType(Class<? extends Fragment> fragmentClass, String title) {
        this.fragmentClass = fragmentClass;
        this.title = title;
    }

    public Fragment createFragment(FragmentActivity currentActivity) {
        return Fragment.instantiate(currentActivity, getFragmentClass().getName());
    }

    public Class<? extends Fragment> getFragmentClass() {
        return fragmentClass;
    }

    public String getTitle() {
        return title;
    }
}
