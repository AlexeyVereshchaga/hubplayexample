package com.hubplay.utils;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;

import com.hubplay.view.fragment.home.tab.EventsFragment;
import com.hubplay.view.fragment.home.tab.FeaturedFragment;
import com.hubplay.view.fragment.home.tab.SportsFragment;

/**
 * 11.07.16.
 *
 * @author Alexey Vereshchaga
 */
public enum StoryType {
    FEATURED(FeaturedFragment.class, "Featured"),
    SPORTS(SportsFragment.class, "Sports");

    private Class<? extends EventsFragment> fragmentClass;
    private String title;

    StoryType(Class<? extends EventsFragment> fragmentClass, String title) {
        this.fragmentClass = fragmentClass;
        this.title = title;
    }

    public Fragment createFragment(FragmentActivity currentActivity) {
        return Fragment.instantiate(currentActivity, getFragmentClass().getName());
    }

    public Class<? extends EventsFragment> getFragmentClass() {
        return fragmentClass;
    }

    public String getTitle() {
        return title;
    }
}
