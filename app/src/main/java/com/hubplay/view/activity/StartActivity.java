package com.hubplay.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.hubplay.R;
import com.hubplay.controller.Screen;
import com.hubplay.controller.TransitionsManager;
import com.hubplay.utils.SharedPreferenceHelper;

/**
 * @author Alexey Vereshchaga
 */
public class StartActivity extends BaseActivity {
    @Override
    public int getLayoutId() {
        return R.layout.activity_start;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TransitionsManager.getInstance().init(this);
        TransitionsManager.getInstance().addFragment(SharedPreferenceHelper.getAccessToken().isEmpty() ? Screen.LOGIN : Screen.SPLASH);
        getToolbarController().setToolbarButton(R.drawable.ic_back_button, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public int getFragmentContainerId() {
        return R.id.fragment_container;
    }

}
