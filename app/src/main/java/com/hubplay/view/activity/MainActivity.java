package com.hubplay.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.DrawerLayout;
import android.text.Html;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hubplay.R;
import com.hubplay.controller.Screen;
import com.hubplay.controller.TransitionsManager;
import com.hubplay.network.model.response.User;
import com.hubplay.utils.Constant;
import com.hubplay.utils.HelpUtils;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 20.04.16.
 *
 * @author Alexey Vereshchaga
 */
public class MainActivity extends BaseActivity {
    // Drawer
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.left_drawer)
    LinearLayout llLeftDrawer;
    @BindView(R.id.tv_drawer_header_text)
    TextView tvDrawerHeader;
    private User user;

    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TransitionsManager.getInstance().init(this);
        getToolbarController().setToolbarButton(R.drawable.ic_action_menu, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                drawerLayout.openDrawer(llLeftDrawer);
            }
        });
        if (savedInstanceState == null) {
            TransitionsManager.getInstance().addFragment(Screen.HOME);
        }
        if (getIntent().getExtras() != null && getIntent().getExtras().getString(Constant.USER) != null) {
            user = HelpUtils.readStringToObject(
                    getIntent().getExtras().getString(Constant.USER),
                    User.class);
        }
        initDrawerViews();
    }

    private void initDrawerViews() {
        tvDrawerHeader.setText(Html.fromHtml(getString(R.string.drawer_hub_play)));
    }

    @Override
    public int getFragmentContainerId() {
        return R.id.fragment_container;
    }

//    @Override
//    public void onBackPressed() {
//        //todo temporal logic
//        Fragment fragment = getSupportFragmentManager().findFragmentByTag(AnswerFragment.class.getName());
//        if (fragment != null) {
//            getSupportFragmentManager().beginTransaction().remove(fragment).commit();
//        }
//        super.onBackPressed();
//    }

    @OnClick(R.id.ll_home)
    void clickHome() {
        TransitionsManager.getInstance().popBackStackToFirst();
        drawerLayout.closeDrawer(llLeftDrawer);
    }

    @OnClick(R.id.ll_account)
    void clickAccount() {
        TransitionsManager.getInstance().popBackStackToFirst();
        TransitionsManager.getInstance().replaceFragment(Screen.ACCOUNT);
        drawerLayout.closeDrawer(llLeftDrawer);
    }

    @OnClick(R.id.ll_logout)
    void clickLogout() {
        TransitionsManager.logout(this);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
