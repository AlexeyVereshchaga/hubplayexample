package com.hubplay.view.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.hubplay.R;
import com.hubplay.controller.ToolbarController;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 20.04.16.
 *
 * @author Alexey Vereshchaga
 */
public abstract class BaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private ToolbarController toolbarController;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());
        ButterKnife.bind(this);
        initToolbar();
        toolbarController = new ToolbarController(toolbar);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        View view = getLayoutInflater().inflate(R.layout.toolbar_layout, null);
        getSupportActionBar().setCustomView(view, new ActionBar.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        getSupportActionBar().setDisplayShowCustomEnabled(true);
    }

    public abstract int getLayoutId();

    public abstract int getFragmentContainerId();

    public ToolbarController getToolbarController() {
        return toolbarController;
    }
}
