package com.hubplay.view.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.view.PagerAdapter;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.hubplay.R;

import java.util.List;

/**
 * 21.04.16.
 *
 * @author Alexey Vereshchaga
 */
public class TutorialAdapter extends PagerAdapter {

    public static final int MAX_COUNT = 1000;

    private Context mContext;
    private List<Sheet> sheets;
    private SpannableString firstText;

    public TutorialAdapter(Context mContext, List<Sheet> sheets) {
        this.mContext = mContext;
        this.sheets = sheets;
        firstText = createSpannableString();
    }

    @Override
    public int getCount() {
        return MAX_COUNT;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        int virtualPosition = position % sheets.size();

        View itemView = LayoutInflater.from(mContext).inflate(R.layout.item_tutorial, container, false);

        ImageView ivBackground = (ImageView) itemView.findViewById(R.id.iv_background);
        TextView tvFirstText = (TextView) itemView.findViewById(R.id.tv_first_text);
        ImageView ivTutorialIcon = (ImageView) itemView.findViewById(R.id.iv_sheet_ic);
        TextView tvDescription = (TextView) itemView.findViewById(R.id.tv_description);

        tvFirstText.setText(firstText);
        ivBackground.setImageResource(sheets.get(virtualPosition).getBackId());
        ivTutorialIcon.setImageResource(sheets.get(virtualPosition).getIconId());
        tvDescription.setText(Html.fromHtml(sheets.get(virtualPosition).getDescription().toUpperCase()));

        container.addView(itemView);
        return itemView;
    }

    private SpannableString createSpannableString() {
        int[] firstSpannable = {14, 19};
        int[] secondSpannable = {38, 41};
        SpannableString spannable = new SpannableString(mContext.getResources().getString(R.string.tutorial_first_text));
        spannable.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.common_blue_1)),
                firstSpannable[0], firstSpannable[1], Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(new ForegroundColorSpan(mContext.getResources().getColor(R.color.common_blue_1)),
                secondSpannable[0], secondSpannable[1], Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(new StyleSpan(Typeface.ITALIC), firstSpannable[0], firstSpannable[1], Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannable.setSpan(new StyleSpan(Typeface.ITALIC), secondSpannable[0], secondSpannable[1], Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;
    }

    @Override
    public void destroyItem(ViewGroup container,
                            int position,
                            Object object) {
        container.removeView((RelativeLayout) object);
        unbindDrawables((View) object);
        object = null;
    }

    protected void unbindDrawables(View view) {
        if (view.getBackground() != null) {
            view.getBackground().setCallback(null);
        }
        if (view instanceof ViewGroup) {
            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
                unbindDrawables(((ViewGroup) view).getChildAt(i));
            }
            ((ViewGroup) view).removeAllViews();
        }
    }


    public static class Sheet {
        private int backId;
        private int iconId;
        private String description;

        public Sheet(int backId, int iconId, String description) {
            this.description = description;
            this.backId = backId;
            this.iconId = iconId;
        }

        public int getBackId() {
            return backId;
        }

        public int getIconId() {
            return iconId;
        }

        public String getDescription() {
            return description;
        }
    }
}

