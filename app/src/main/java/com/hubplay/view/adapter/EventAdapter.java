package com.hubplay.view.adapter;

import android.content.Context;
import android.support.v4.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hubplay.R;
import com.hubplay.network.model.response.Event;
import com.hubplay.network.model.response.Team;
import com.hubplay.utils.HelpUtils;
import com.hubplay.utils.view.ExpandAnimation;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 07.06.16.
 *
 * @author Alexey Vereshchaga
 */
public class EventAdapter extends ArrayAdapter<Event> {

    private static final String TIME_TEMPLATE = "h:mm aa";
    private static final int DEFAULT_TEAMS_NUMBER = 2;
    private static final int ANIMATION_DURATION = 300;
    private int resource;
    private Pair<Integer, Integer> currentPositionState;
    private Boolean checkingFlag = false;

    public EventAdapter(Context context, int resource) {
        super(context, resource);
        this.resource = resource;
    }

    static class ViewHolder {
        @BindView(R.id.rg_payment_type)
        RadioGroup rgPaymentType;
        @BindView(R.id.rb_free)
        RadioButton rbFree;
        @BindView(R.id.rb_play)
        RadioButton rbPaid;
        @BindView(R.id.tv_start_time)
        TextView tvStartTime;
        @BindView(R.id.ll_hosted_by)
        LinearLayout llHosted;
        @BindView(R.id.iv_hosted_by)
        ImageView ivHostedBy;
        @BindView(R.id.tv_hosted_by)
        TextView tvHostedBy;
        @BindView(R.id.iv_first_team)
        ImageView ivFirstTeam;
        @BindView(R.id.iv_second_team)
        ImageView ivSecondTeam;
        @BindView(R.id.tv_first_team_name)
        TextView tvFirstTeam;
        @BindView(R.id.tv_second_team_name)
        TextView tvSecondTeam;
        @BindView(R.id.rl_teams)
        RelativeLayout rlTeams;
        @BindView(R.id.ll_playing_place)
        LinearLayout llPlayingPlace;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {
            rowView = LayoutInflater.from(getContext()).inflate(resource, null);
            holder = new ViewHolder(rowView);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        checkingFlag = true;
        if (currentPositionState != null && position == currentPositionState.first) {
            holder.llPlayingPlace.setVisibility(View.VISIBLE);
            switch (currentPositionState.second) {
                case R.id.rb_free:
                    holder.rbFree.setChecked(true);
                    break;
                case R.id.rb_play:
                    holder.rbPaid.setChecked(true);
                    break;
            }
        } else {
            holder.llPlayingPlace.setVisibility(View.GONE);
            holder.rbFree.setChecked(false);
            holder.rbPaid.setChecked(false);
        }
        checkingFlag = false;

        Event event = getItem(position);
        Date startDate = event.getStartDate();
        if (startDate != null) {
            holder.tvStartTime.setText(String.format(Locale.US, getContext().getString(R.string.event_start_time), new SimpleDateFormat(TIME_TEMPLATE, Locale.US).format(startDate)));
        }
        if (event.getTeams() != null && event.getTeams().size() == DEFAULT_TEAMS_NUMBER) {
            holder.rlTeams.setVisibility(View.VISIBLE);
            Team firstTeam = event.getTeams().get(0);
            Team secondTeam = event.getTeams().get(1);
            Glide
                    .with(getContext())
                    .load(firstTeam.getIcon())
                    .into(holder.ivFirstTeam);
            Glide
                    .with(getContext())
                    .load(secondTeam.getIcon())
                    .into(holder.ivSecondTeam);
            holder.tvFirstTeam.setText(firstTeam.getTitle());
            holder.tvSecondTeam.setText(secondTeam.getTitle());
        } else {
            holder.rlTeams.setVisibility(View.GONE);
        }
        if (event.getHostedBy() != null) {
            holder.llHosted.setVisibility(View.VISIBLE);
            holder.ivHostedBy.setVisibility(View.VISIBLE);
            if (event.getHostedBy().getIcon() != null) {
                Glide
                        .with(getContext())
                        .load(event.getHostedBy().getIcon())
                        .into(holder.ivHostedBy);
            } else {
                holder.ivHostedBy.setVisibility(View.GONE);
            }
            holder.tvHostedBy.setText(event.getHostedBy().getFullName());
        } else {
            holder.llHosted.setVisibility(View.GONE);
        }
        holder.rgPaymentType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (group.getCheckedRadioButtonId() != -1 && !checkingFlag) {
                    if (holder.llPlayingPlace.getVisibility() == View.GONE) {
                        if (currentPositionState != null) {
                            View view = HelpUtils.getViewByPosition(currentPositionState.first, (ListView) parent);
                            RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.rg_payment_type);
                            LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.ll_playing_place);
                            radioGroup.clearCheck();
                            ExpandAnimation animation = new ExpandAnimation(linearLayout,
                                    ANIMATION_DURATION,
                                    ExpandAnimation.COLLAPSE,
                                    (int) getContext().getResources().getDimension(R.dimen.event_place_layout_height));
                            linearLayout.startAnimation(animation);
                        }
                        ExpandAnimation animation = new ExpandAnimation(holder.llPlayingPlace,
                                ANIMATION_DURATION,
                                ExpandAnimation.EXPAND,
                                (int) getContext().getResources().getDimension(R.dimen.event_place_layout_height));
                        holder.llPlayingPlace.startAnimation(animation);
                    }
                    currentPositionState = new Pair<>(position, checkedId);
                }
            }
        });
        return rowView;
    }


}
