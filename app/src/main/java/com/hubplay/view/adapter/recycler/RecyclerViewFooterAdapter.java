package com.hubplay.view.adapter.recycler;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.hubplay.R;
import com.hubplay.network.model.response.Event;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 18.07.16.
 *
 * @author Alexey Vereshchaga
 */
public class RecyclerViewFooterAdapter extends AbstractRecyclerViewFooterAdapter<Event> {

    private OnItemViewClickListener clickListener;

    public RecyclerViewFooterAdapter(RecyclerView recyclerView, List<Event> eventList, OnLoadMoreListener onLoadMoreListener) {
        super(recyclerView, eventList, onLoadMoreListener);
    }

    @Override
    public RecyclerView.ViewHolder onCreateBasicItemViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_recycler_event, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public RecyclerView.ViewHolder onCreateFooterViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.progress_bar, parent, false);
        return new ProgressViewHolder(v);
    }

    @Override
    public void onBindBasicItemView(RecyclerView.ViewHolder genericHolder, final int position) {
        final ViewHolder holder = (ViewHolder) genericHolder;
        final Event event = getItem(position);
        if (clickListener != null) {
            View.OnClickListener onClickListener = new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    clickListener.onViewClick(v, position);
                }
            };
            holder.ibPlay.setOnClickListener(onClickListener);
            holder.btnMore.setOnClickListener(onClickListener);
        }
        if (event != null) {
            holder.tvTitle.setText(event.getTitle());
            String subtitle = event.getSubtitle();
            holder.tvSubtitle.setText(TextUtils.isEmpty(subtitle) ?
                    holder.tvSubtitle
                            .getResources()
                            .getString(R.string.event_default_subtitle) : subtitle);
            RequestManager requestManager = Glide.with(holder.ivBgIcon.getContext());
            requestManager
                    .load(event.getBgIcon())
                    .centerCrop()
                    .into(holder.ivBgIcon);
            if (!TextUtils.isEmpty(event.getIcon())) {
                holder.ivIcon.setVisibility(View.VISIBLE);
                requestManager
                        .load(event.getIcon())
                        .into(holder.ivIcon);
            } else {
                holder.ivIcon.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onBindFooterView(RecyclerView.ViewHolder genericHolder, int position) {
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.btn_more)
        public Button btnMore;
        @BindView(R.id.ib_play)
        public ImageButton ibPlay;
        @BindView(R.id.iv_icon_bg)
        public ImageView ivBgIcon;
        @BindView(R.id.iv_icon)
        public ImageView ivIcon;
        @BindView(R.id.tv_title)
        public TextView tvTitle;
        @BindView(R.id.tv_subtitle)
        public TextView tvSubtitle;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.progressBar)
        public ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    public void setClickListener(OnItemViewClickListener clickListener) {
        this.clickListener = clickListener;
    }


    public interface OnItemViewClickListener {
        void onViewClick(View view, int position);
    }
}
