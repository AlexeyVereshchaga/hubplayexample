package com.hubplay.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hubplay.R;
import com.hubplay.network.model.response.League;

/**
 * 15.07.16.
 *
 * @author Alexey Vereshchaga
 */
public class SportSpinnerAdapter extends ArrayAdapter<League> {
    public SportSpinnerAdapter(Context context, int resource) {
        super(context, resource);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return makeLayout(position, convertView, parent, R.layout.item_spinner);
    }

    @Override
    public View getDropDownView(final int position, final View convertView,
                                final ViewGroup parent) {
        return makeDropDownLayout(position, convertView, parent,
                R.layout.item_spinner_drop_down);
    }

    private View makeLayout(int position, View convertView,
                            ViewGroup parent, int layout) {
        TextView tv;
        League league = getItem(position);
        if (convertView != null) {
            tv = (TextView) convertView;
        } else {
            tv = (TextView) LayoutInflater.from(getContext()).inflate(layout,
                    parent, false);
        }
        tv.setText(getItem(position) != null ? league.getTitle() : getContext().getResources().getString(R.string.all_leagues));
        return tv;
    }

    private View makeDropDownLayout(int position, View convertView,
                                    ViewGroup parent, int layout) {
        LinearLayout ll;
        TextView tv;
        League league = getItem(position);
        if (convertView != null) {
            ll = (LinearLayout) convertView;
            tv = (TextView) convertView.findViewById(R.id.tv_sport_spinner);
        } else {
            View view = LayoutInflater.from(getContext()).inflate(layout, parent, false);
            ll = (LinearLayout) LayoutInflater.from(getContext()).inflate(layout, parent, false);
            tv = (TextView) ll.findViewById(R.id.tv_sport_spinner);
        }
        tv.setText(getItem(position) != null ? league.getTitle() : getContext().getResources().getString(R.string.all_leagues));
        return ll;
    }
}
