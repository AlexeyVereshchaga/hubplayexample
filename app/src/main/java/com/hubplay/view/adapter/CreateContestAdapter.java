package com.hubplay.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.hubplay.utils.ContestType;

/**
 * 03.06.16.
 *
 * @author Alexey Vereshchaga
 */
public class CreateContestAdapter extends FragmentPagerAdapter {
    private FragmentActivity activity;
    private ContestType[] tabs;

    public CreateContestAdapter(FragmentManager fm, FragmentActivity activity) {
        super(fm);
        tabs = ContestType.values();
        this.activity = activity;
    }

    @Override
    public Fragment getItem(int position) {
        return tabs[position].createFragment(activity);
    }

    @Override
    public int getCount() {
        return tabs.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabs[position].getTitle();
    }
}
