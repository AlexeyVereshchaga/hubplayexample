package com.hubplay.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hubplay.R;
import com.hubplay.network.model.response.League;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 03.06.16.
 *
 * @author Alexey Vereshchaga
 */
public class LeagueAdapter extends ArrayAdapter<League> {
    private int resource;

    public LeagueAdapter(Context context, int resource) {
        super(context, resource);
        this.resource = resource;
    }

    static class ViewHolder {
        @BindView(R.id.iv_league)
        ImageView ivLeague;
        @BindView(R.id.tv_league_name)
        TextView tvLeagueName;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {
            rowView = LayoutInflater.from(getContext()).inflate(resource, null);
            holder = new ViewHolder(rowView);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        Glide
                .with(getContext())
                .load(getItem(position).getIcon())
                .into(holder.ivLeague);
        holder.tvLeagueName.setText(getItem(position).getTitle());
        return rowView;
    }
}
