package com.hubplay.view.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hubplay.R;
import com.hubplay.network.model.response.Answer;

import java.util.ArrayList;
import java.util.List;

/**
 * 26.04.16.
 *
 * @author Alexey Vereshchaga
 */
public class AnswerAdapter extends RecyclerView.Adapter<AnswerAdapter.ViewHolder> {
    private List<Answer> answers;
    private final static String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    private OnItemClickListener onItemClickListener;
    private boolean itemsClickable = true;

    public AnswerAdapter() {
    }

    public AnswerAdapter(List<Answer> answers) {
        this.answers = answers;
    }

    @Override
    public AnswerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_answer, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final AnswerAdapter.ViewHolder holder, final int position) {
        holder.tvAnswerLetter.setText(String.valueOf(ALPHABET.charAt(position)));
        holder.tvAnswer.setText(answers.get(position).getText());
        if (onItemClickListener != null) {
            holder.llAnswer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (itemsClickable) {
                        onItemClickListener.onClick(holder.getAdapterPosition());
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return answers.size();
    }

    public boolean isItemsClickable() {
        return itemsClickable;
    }

    public void setItemsClickable(boolean itemsClickable) {
        this.itemsClickable = itemsClickable;
    }

    public void setAnswers(List<Answer> answers) {
        List<Answer> answers1 = new ArrayList<>(answers);
        this.answers = answers1;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public void clearData() {
        answers.clear();
        notifyDataSetChanged();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        public LinearLayout llAnswer;
        public TextView tvAnswerLetter;
        public TextView tvAnswer;

        public ViewHolder(View itemView) {
            super(itemView);
            llAnswer = (LinearLayout) itemView.findViewById(R.id.ll_answer);
            tvAnswerLetter = (TextView) itemView.findViewById(R.id.iv_answer_letter);
            tvAnswer = (TextView) itemView.findViewById(R.id.tv_answer);
        }
    }

    public Answer getItem(int position) {
        return answers.get(position);
    }

    public interface OnItemClickListener {
        void onClick(int position);
    }
}