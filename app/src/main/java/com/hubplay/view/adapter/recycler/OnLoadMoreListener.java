package com.hubplay.view.adapter.recycler;

/**
 * 18.07.16.
 *
 * @author Alexey Vereshchaga
 */
public interface OnLoadMoreListener {
    void onLoadMore();
}
