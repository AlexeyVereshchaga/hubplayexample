package com.hubplay.view.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.hubplay.utils.StoryType;

import java.util.Arrays;

/**
 * 11.07.16.
 *
 * @author Alexey Vereshchaga
 */
public class HomePagerAdapter extends FragmentPagerAdapter {
    private FragmentActivity activity;
    private StoryType[] tabs;

    public HomePagerAdapter(FragmentManager fm, FragmentActivity activity) {
        super(fm);
        tabs = StoryType.values();
        this.activity = activity;
    }

    @Override
    public Fragment getItem(int position) {
        return tabs[position].createFragment(activity);
    }

    @Override
    public int getCount() {
        return tabs.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabs[position].getTitle();
    }

    public Fragment getItem(StoryType storyType) {
        return getItem(Arrays.asList(tabs).indexOf(storyType));
    }
}
