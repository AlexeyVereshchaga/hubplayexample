package com.hubplay.view.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hubplay.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 01.06.16.
 *
 * @author Alexey Vereshchaga
 */
public class AccountAdapter extends ArrayAdapter<String> {
    private int resource;

    public AccountAdapter(Context context, int resource, String[] objects) {
        super(context, resource, objects);
        this.resource = resource;
    }

    static class ViewHolder {
        @BindView(R.id.ll_item_account)
        LinearLayout llItemAccount;
        @BindView(R.id.tv_item_name)
        TextView tvItemName;
        @BindView(R.id.iv_arrow_right)
        ImageView ivArrowRight;
        @BindView(R.id.tv_item_letter)
        TextView tvItemLetter;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {
            rowView = LayoutInflater.from(getContext()).inflate(resource, null);
            holder = new ViewHolder(rowView);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        switch (getItem(position)) {
            case "Story Menu":
                createListHeader(holder);
                break;
            case "Support":
                createListHeader(holder);
                break;
            default:
                createDefaultItem(holder, position);
                break;
        }
        holder.tvItemName.setText(getItem(position));
        return rowView;
    }

    private void createDefaultItem(ViewHolder holder, int position) {
        if (getContext() != null) {
            holder.llItemAccount.setBackgroundColor(getContext().getResources().getColor(R.color.common_item_bg));
            int paddingHorizontalPx = (int) getContext().getResources().getDimension(R.dimen.item_common_padding_horizontal);
            int paddingVerticalPx = (int) getContext().getResources().getDimension(R.dimen.item_common_padding_vertical);
            holder.llItemAccount.setPadding(paddingHorizontalPx, paddingVerticalPx, paddingHorizontalPx, paddingVerticalPx);
            holder.llItemAccount.setClickable(false);
            holder.tvItemName.setAllCaps(true);
            holder.tvItemName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getContext().getResources().getDimension(R.dimen.item_account_text_size));
            holder.ivArrowRight.setVisibility(View.VISIBLE);
            holder.tvItemLetter.setVisibility(View.VISIBLE);
            holder.tvItemLetter.setText("" + getItem(position).charAt(0));
        }
    }

    private void createListHeader(ViewHolder holder) {
        if (getContext() != null) {
            holder.llItemAccount.setBackgroundColor(Color.TRANSPARENT);
            int paddingHorizontalPx = (int) getContext().getResources().getDimension(R.dimen.item_common_padding_horizontal);
            int paddingVerticalPx = (int) getContext().getResources().getDimension(R.dimen.account_item_header_padding);
            holder.llItemAccount.setPadding(paddingHorizontalPx, paddingVerticalPx, paddingHorizontalPx, paddingVerticalPx);
            holder.llItemAccount.setClickable(true);
            holder.tvItemName.setAllCaps(false);
            holder.tvItemName.setTextSize(TypedValue.COMPLEX_UNIT_PX, getContext().getResources().getDimension(R.dimen.item_account_header_text_size));
            holder.ivArrowRight.setVisibility(View.GONE);
            holder.tvItemLetter.setVisibility(View.GONE);
        }
    }
}
