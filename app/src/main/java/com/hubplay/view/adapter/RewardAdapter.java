package com.hubplay.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hubplay.R;
import com.hubplay.network.model.response.Reward;
import com.hubplay.utils.HelpUtils;
import com.hubplay.utils.view.ExpandAnimation;

import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 24.06.16.
 *
 * @author Alexey Vereshchaga
 */
public class RewardAdapter extends ArrayAdapter<Reward> {

    private static final int ANIMATION_DURATION = 300;
    private int resource;
    private int activeItemPos = -1;

    public RewardAdapter(Context context, int resource) {
        super(context, resource);
        this.resource = resource;
    }

    static class ViewHolder {
        @BindView(R.id.reward_main)
        LinearLayout llRewardMain;
        @BindView(R.id.tv_reward_title)
        TextView tvTitle;
        @BindView(R.id.iv_reward_image)
        ImageView ivReward;
        @BindView(R.id.tv_reward_expires)
        TextView tvRewardExpires;
        @BindView(R.id.tv_redeem)
        TextView tvRedeem;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    @Override
    public View getView(final int position, View convertView, final ViewGroup parent) {
        final ViewHolder holder;
        View rowView = convertView;
        if (rowView == null) {
            rowView = LayoutInflater.from(getContext()).inflate(resource, null);
            holder = new ViewHolder(rowView);
            rowView.setTag(holder);
        } else {
            holder = (ViewHolder) rowView.getTag();
        }
        Reward reward = getItem(position);
        Glide
                .with(getContext())
                .load(reward.getIcon())
                .into(holder.ivReward);
        if (reward.getExpirationDate() == null) {
            reward.setExpirationDate(new Date(0));
        }
        holder.tvTitle.setText(reward.getTitle());
        holder.tvRewardExpires.setText(String.format(
                getContext().getResources().getString(R.string.reward_expires_in),
                HelpUtils.getNumberOfDays(reward.getExpirationDate())));
        holder.tvRedeem.setVisibility(activeItemPos == position ? View.VISIBLE : View.GONE);

        holder.llRewardMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (activeItemPos != -1 && activeItemPos != position) {
                    View view = HelpUtils.getViewByPosition(activeItemPos, (ListView) parent);
                    TextView tvRedeem = (TextView) view.findViewById(R.id.tv_redeem);
                    ExpandAnimation animation = new ExpandAnimation(tvRedeem,
                            ANIMATION_DURATION,
                            ExpandAnimation.COLLAPSE,
                            (int) getContext().getResources().getDimension(R.dimen.reward_redeem_height));
                    tvRedeem.startAnimation(animation);
                }
                if (holder.tvRedeem.getVisibility() != View.VISIBLE) {
                    ExpandAnimation animation = new ExpandAnimation(holder.tvRedeem,
                            ANIMATION_DURATION,
                            ExpandAnimation.EXPAND,
                            Math.round(getContext().getResources().getDimension(R.dimen.reward_redeem_height)));
                    holder.tvRedeem.startAnimation(animation);
                }
                activeItemPos = position;
            }
        });
        return rowView;
    }
}
