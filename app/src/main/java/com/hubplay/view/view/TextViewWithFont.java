package com.hubplay.view.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.os.Build;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.TextView;

import com.hubplay.R;

public class TextViewWithFont extends TextView {

    public static final String TAG = TextViewWithFont.class.getName();
    public static final String FONTS_DIRECTORY = "fonts/";

    private String mFont;
    private Typeface mTypeface;

    public TextViewWithFont(Context context) {
        super(context);
    }

    public TextViewWithFont(Context context, AttributeSet attrs) {
        super(context, attrs);
        initAttributes(context, attrs);
        setTypeface(context, attrs);
    }

    public TextViewWithFont(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initAttributes(context, attrs);
        setTypeface(context, attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public TextViewWithFont(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initAttributes(context, attrs);
        setTypeface(context, attrs);
    }

    protected void initAttributes(Context context, AttributeSet attrs) {
        TypedArray ta = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.TextViewWithFont,
                0, 0);
        try {
            mFont = ta.getString(R.styleable.TextViewWithFont_font);
        } finally {
            ta.recycle();
        }
    }

    private void setTypeface(Context context, AttributeSet attrs) {
        if (!TextUtils.isEmpty(mFont)) {
            try {
                mTypeface = Typeface.createFromAsset(context.getAssets(), FONTS_DIRECTORY.concat(mFont));
            } catch (Exception e) {
                Log.e(TAG, e.getMessage());
            }
        }
        if (mTypeface != null) {
            this.setTypeface(mTypeface);
        }
    }
}
