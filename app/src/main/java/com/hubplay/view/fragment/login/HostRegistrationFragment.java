package com.hubplay.view.fragment.login;

import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.hubplay.R;
import com.hubplay.controller.Screen;
import com.hubplay.controller.TransitionsManager;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 08.07.16.
 *
 * @author Alexey Vereshchaga
 */
public class HostRegistrationFragment extends RegistrationFragment {

    public static final String HOST_LOGIN_FLAG_KEY = "host_login_flag_key";

    @BindView(R.id.btn_register_request)
    Button btnHostRegister;
    @BindView(R.id.btn_host_register_switch)
    Button btnRegister;

    @Override
    protected void initView(View view) {
        btnHostRegister.setText(R.string.host_login_host_register);
        btnRegister.setText(R.string.login_register);
    }

    @OnClick(R.id.btn_register_request)
    void onClickHostRegister() {
        Toast.makeText(getActivity(), R.string.this_feature_is_coming_soon, Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.btn_host_register_switch)
    void onClickRegister() {
        if (getArguments() != null && getArguments().getBoolean(HOST_LOGIN_FLAG_KEY)) {
            TransitionsManager.getInstance().popBackStackToFirst();
            TransitionsManager.getInstance().replaceFragment(Screen.REGISTRATION);
        } else {
            getActivity().onBackPressed();
        }
    }
}

