package com.hubplay.view.fragment.create_contest.tab;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hubplay.R;
import com.hubplay.controller.DataManager;
import com.hubplay.controller.Screen;
import com.hubplay.controller.TransitionsManager;
import com.hubplay.network.BaseHandler;
import com.hubplay.network.BaseRemoteListener;
import com.hubplay.network.model.ErrorModel;
import com.hubplay.network.model.response.League;
import com.hubplay.view.activity.MainActivity;
import com.hubplay.view.adapter.LeagueAdapter;
import com.hubplay.view.fragment.EventsFragment;

import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 03.06.16.
 *
 * @author Alexey Vereshchaga
 */
public class LeagueContestFragment extends Fragment {

    @BindView(R.id.leagues_list)
    ListView lvLeagues;
    private List<League> leagues;

    private LeagueAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getViewLayout(), container, false);
        ButterKnife.bind(this, view);
        initView(view);
        return view;
    }

    protected int getViewLayout() {
        return R.layout.fragment_leagues;
    }

    protected void initView(View view) {
        adapter = new LeagueAdapter(getContext(), R.layout.item_league);
        lvLeagues.setAdapter(adapter);
        if (leagues != null) {
            adapter.addAll(leagues);
        }
        lvLeagues.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putInt(EventsFragment.LEAGUE_ID_KEY, adapter.getItem(position).getId());
                TransitionsManager.getInstance().replaceFragment(Screen.CHOOSE_GAME, bundle);
            }
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainActivity activity = ((MainActivity) getActivity());
        Integer points = activity.getUser().getPointBalance();
        String pointsStr = String.format(Locale.US, getResources().getString(R.string.header_hub_points), points);
        activity.getToolbarController().setRightText(pointsStr);
        DataManager.getLeagues(new BaseHandler<>(new BaseRemoteListener<List<League>, ErrorModel>(getActivity()) {
            @Override
            public void onSuccess(List<League> result) {
                leagues = result;
                if (adapter != null) {
                    adapter.addAll(leagues);
                }
            }
        }, new TypeReference<ErrorModel>() {
        }));
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).getToolbarController().setTitle(getResources().getString(R.string.fragment_join_story));
    }
}
