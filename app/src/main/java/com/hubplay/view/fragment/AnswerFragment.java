package com.hubplay.view.fragment;

import android.content.Context;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hubplay.R;
import com.hubplay.view.activity.MainActivity;

import java.util.Locale;

import butterknife.BindView;

/**
 * 26.04.16.
 *
 * @author Alexey Vereshchaga
 */
public class AnswerFragment extends AbstractFragment<MainActivity> {

    public static final String ANSWER_KEY = "answer_key";
    private boolean answer;

    @BindView(R.id.iv_answer)
    ImageView ivAnswer;
    @BindView(R.id.tv_balance)
    TextView tvBalance;
    @BindView(R.id.tv_answer)
    TextView tvAnswer;
    @BindView(R.id.tv_points)
    TextView tvPoints;

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_answer;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        answer = getArguments().getBoolean(ANSWER_KEY);
    }

    @Override
    protected void initView(View view) {
        int bgResId = answer ? R.drawable.answer_correct_bg : R.drawable.answer_wrong_bg;
        ivAnswer.setBackground(getResources().getDrawable(bgResId));
        tvBalance.setText(String.format(Locale.US, getResources().getString(R.string.answer_balance), answer ? 50 : 30));
        tvAnswer.setText(answer ? "Correct!" : "Wrong!");
        String answerPoints = String.format(Locale.US, getResources().getString(R.string.answer_points), answer ? "+10" : "-10");
        SpannableString spannableString = new SpannableString(answerPoints);
        int colorRes = answer ? R.color.answer_correct : R.color.answer_wrong;
        spannableString.setSpan(new ForegroundColorSpan(getResources().getColor(colorRes)), 0, 3, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvPoints.setText(spannableString);
    }

    @Override
    public void onResume() {
        super.onResume();
        getBaseActivity().getToolbarController().setTitle("IOC | ChrisTach");
        ((Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE)).vibrate(800);
    }
}
