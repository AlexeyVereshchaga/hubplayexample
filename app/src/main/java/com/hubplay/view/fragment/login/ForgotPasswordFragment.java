package com.hubplay.view.fragment.login;

import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hubplay.R;
import com.hubplay.controller.DataManager;
import com.hubplay.network.BaseHandler;
import com.hubplay.network.BaseRemoteListener;
import com.hubplay.network.model.ErrorModel;
import com.hubplay.utils.HelpUtils;
import com.hubplay.view.activity.StartActivity;
import com.hubplay.view.fragment.AbstractFragment;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 08.07.16.
 *
 * @author Alexey Vereshchaga
 */
public class ForgotPasswordFragment extends AbstractFragment<StartActivity> {
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_forgot_password;
    }

    @OnClick(R.id.btn_restore_password)
    void forgotPassword() {
        int invalidEmailMessageId = HelpUtils.validateEmail(etEmail.getText().toString());
        if (invalidEmailMessageId != -1) {
            Toast.makeText(getActivity(), invalidEmailMessageId, Toast.LENGTH_LONG).show();
            return;
        }
        DataManager.forgotPassword(etEmail.getText().toString(),
                new BaseHandler<>(new BaseRemoteListener<Void, ErrorModel>(getActivity()) {

                    @Override
                    public void onStartTask() {
                        progressBar.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onSuccess(Void result) {
                        //// TODO: 06.07.16 temporary message
                        Toast.makeText(getActivity(), "Success!", Toast.LENGTH_LONG).show();
                        hideKeyboard();
                    }

                    @Override
                    public void onFinishTask() {
                        progressBar.setVisibility(View.GONE);
                    }
                }, new TypeReference<ErrorModel>() {
                }));
    }

    @Override
    public void onResume() {
        super.onResume();
        getBaseActivity().getToolbarController().setTitle(getResources().getString(R.string.forgot_password));
    }
}
