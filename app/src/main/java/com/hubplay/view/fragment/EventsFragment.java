package com.hubplay.view.fragment;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RadioGroup;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hubplay.R;
import com.hubplay.controller.DataManager;
import com.hubplay.controller.Screen;
import com.hubplay.controller.TransitionsManager;
import com.hubplay.network.BaseHandler;
import com.hubplay.network.BaseRemoteListener;
import com.hubplay.network.model.ErrorModel;
import com.hubplay.network.model.response.Event;
import com.hubplay.utils.Constant;
import com.hubplay.utils.ContestPaymentType;
import com.hubplay.utils.HelpUtils;
import com.hubplay.view.activity.MainActivity;
import com.hubplay.view.adapter.EventAdapter;

import java.util.List;

import butterknife.BindView;

/**
 * 07.06.16.
 *
 * @author Alexey Vereshchaga
 */
public class EventsFragment extends AbstractFragment<MainActivity> {
    @BindView(android.R.id.list)
    ListView lvEvents;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swlEvents;
    private int leagueId;
    private EventAdapter adapter;
    private List<Event> events;

    public static final String LEAGUE_ID_KEY = "LEAGUE_ID";

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_events;
    }

    @Override
    protected void initView(View view) {
        adapter = new EventAdapter(getActivity(), R.layout.item_event);
        lvEvents.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        lvEvents.setEmptyView(view.findViewById(android.R.id.empty));
        lvEvents.setAdapter(adapter);
        if (events != null) {
            adapter.clear();
            adapter.addAll(events);
        }
        swlEvents.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getEvents();
            }
        });
        lvEvents.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
//                ContestPaymentType paymentType = getContestPaymentType(view);
//                if (paymentType != null) {
//                    bundle.putString(EventFragment.CONTEST_PAYMENT_TYPE_KEY, paymentType.name());
//                }
                String questionsStr = HelpUtils.createStringFromObject(adapter.getItem(position));
                bundle.putString(Constant.EVENT_KEY, questionsStr);
                TransitionsManager.getInstance().replaceFragment(Screen.CHOOSE_TOOL, bundle);
            }
        });
    }

    @Nullable
    private ContestPaymentType getContestPaymentType(View view) {
        RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.rg_payment_type);
        ContestPaymentType paymentType = null;
        switch (radioGroup.getCheckedRadioButtonId()) {
            case R.id.rb_free:
                paymentType = ContestPaymentType.FREE;
                break;
            case R.id.rb_play:
                paymentType = ContestPaymentType.PAID;
                break;
        }
        return paymentType;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        leagueId = getArguments().getInt(LEAGUE_ID_KEY, -1);
        if (leagueId != -1) {
            getEvents();
        }
    }

    private void getEvents() {
        DataManager.getEventsByDay(leagueId, new BaseHandler<>(new BaseRemoteListener<List<Event>, ErrorModel>(getActivity()) {
            @Override
            public void onStartTask() {
                if (swlEvents != null) {
                    swlEvents.setRefreshing(true);
                }
            }

            @Override
            public void onSuccess(List<Event> result) {
                super.onSuccess(result);
                events = result;
                if (adapter != null) {
                    adapter.clear();
                    adapter.addAll(events);
                }
            }

            @Override
            public void onFinishTask() {
                if (swlEvents != null) {
                    swlEvents.setRefreshing(false);
                }
            }
        }, new TypeReference<ErrorModel>() {
        }));
    }

    @Override
    public void onResume() {
        super.onResume();
        getBaseActivity().getToolbarController().setTitle(getResources().getString(R.string.events_choose_a_story));
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }
}
