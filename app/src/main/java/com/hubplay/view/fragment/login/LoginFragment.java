package com.hubplay.view.fragment.login;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hubplay.R;
import com.hubplay.controller.DataManager;
import com.hubplay.controller.Screen;
import com.hubplay.controller.TransitionsManager;
import com.hubplay.network.BaseHandler;
import com.hubplay.network.BaseRemoteListener;
import com.hubplay.network.model.ErrorModel;
import com.hubplay.network.model.response.Access;
import com.hubplay.network.model.response.User;
import com.hubplay.utils.Constant;
import com.hubplay.utils.HelpUtils;
import com.hubplay.utils.SharedPreferenceHelper;
import com.hubplay.view.activity.MainActivity;
import com.hubplay.view.activity.StartActivity;
import com.hubplay.view.fragment.AbstractFragment;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 06.07.16.
 *
 * @author Alexey Vereshchaga
 */
public class LoginFragment extends AbstractFragment<StartActivity> {
    private static final String TAG = LoginFragment.class.getName();
    @BindView(R.id.et_username)
    EditText etUsername;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.btn_register_switch)
    Button btnRegister;
    @BindView(R.id.tv_login)
    TextView tvLogin;

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_login;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
    }

    private void login() {
        DataManager.login(etUsername.getText().toString(), etPassword.getText().toString(),
                new BaseHandler<>(new BaseRemoteListener<Access, ErrorModel>(getActivity()) {
                    @Override
                    public void onStartTask() {
                        progressBar.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onSuccess(Access result) {
                        Log.d(TAG, result.toString());
                        SharedPreferenceHelper.saveAccessToken(result.getAccessToken());
                        SharedPreferenceHelper.saveRefreshToken(result.getRefreshToken());
                        SharedPreferenceHelper.saveTokenType(result.getTokenType());
                        getUser();
                    }

                    @Override
                    public void onFailure(ErrorModel error) {
                        Toast.makeText(getContext(), error.getErrorDescription(), Toast.LENGTH_LONG).show();
                        HelpUtils.deleteTokens();
                    }


                    @Override
                    public void onError(Throwable t) {
                        super.onError(t);
                        progressBar.setVisibility(View.GONE);
                    }
                }, new TypeReference<ErrorModel>() {
                }));
    }

    private void getUser() {
        DataManager.getUser(new BaseHandler<>(new BaseRemoteListener<User, ErrorModel>(getActivity()) {
            @Override
            public void onStartTask() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(User result) {
                Bundle bundle = new Bundle();
                bundle.putString(Constant.USER, HelpUtils.createStringFromObject(result));
                TransitionsManager.getInstance().startActivity(MainActivity.class, bundle);
            }

            @Override
            public void onFailure(ErrorModel errorModel) {
                Toast.makeText(getContext(), errorModel.getErrorDescription(), Toast.LENGTH_LONG).show();
                HelpUtils.deleteTokens();
            }

            @Override
            public void onFinishTask() {
                progressBar.setVisibility(View.GONE);
            }
        }, new TypeReference<ErrorModel>() {
        }));
    }

    @OnClick(R.id.btn_login_request)
    void onClickLogin() {
        int invalidFieldMessageId = HelpUtils.validateLoginFields(
                etUsername.getText().toString(),
                etPassword.getText().toString());
        if (invalidFieldMessageId != -1) {
            Toast.makeText(getActivity(), invalidFieldMessageId, Toast.LENGTH_LONG).show();
//            return;
        }
        login();
    }

    @OnClick(R.id.btn_register_switch)
    void onClickRegister() {
        TransitionsManager.getInstance().replaceFragment(Screen.REGISTRATION);
    }

    @OnClick(R.id.tv_forgot_password)
    void forgotPassword() {
        TransitionsManager.getInstance().replaceFragment(Screen.FORGOT_PASSWORD);
    }

    @OnClick(R.id.tv_login)
    void onClickHostLogin() {
        TransitionsManager.getInstance().replaceFragment(Screen.HOST_LOGIN);
    }
}
