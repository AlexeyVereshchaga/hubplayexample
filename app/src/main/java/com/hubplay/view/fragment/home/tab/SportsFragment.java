package com.hubplay.view.fragment.home.tab;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hubplay.R;
import com.hubplay.controller.DataManager;
import com.hubplay.network.BaseHandler;
import com.hubplay.network.BaseRemoteListener;
import com.hubplay.network.model.ErrorModel;
import com.hubplay.network.model.response.League;
import com.hubplay.view.adapter.SportSpinnerAdapter;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import butterknife.BindView;

/**
 * 11.07.16.
 *
 * @author Alexey Vereshchaga
 */
public class SportsFragment extends com.hubplay.view.fragment.home.tab.EventsFragment {
    @BindView(R.id.spinner_league_filter)
    Spinner leaguesSpinner;

    private SportSpinnerAdapter spinnerAdapter;
    private Set<League> leagues = new LinkedHashSet<>();
    private Integer leagueId;

    protected int getViewLayout() {
        return R.layout.fragment_sports;
    }

    @Override
    protected void getEventsList() {
        DataManager.getSportsEvents(leagueId, page, callback);
    }

    @Override
    protected void initView() {
        super.initView();
        spinnerAdapter = new SportSpinnerAdapter(getContext(), R.layout.item_spinner);
        spinnerAdapter.setDropDownViewResource(R.layout.item_spinner_drop_down);
        leagues.add(null);
        spinnerAdapter.addAll(leagues);
        leaguesSpinner.setAdapter(spinnerAdapter);
        DataManager.getLeagues(new BaseHandler<>(new BaseRemoteListener<List<League>, ErrorModel>(getActivity()) {
            @Override
            public void onSuccess(List<League> result) {
                leagues.addAll(result);
                if (spinnerAdapter != null) {
                    leagues.addAll(result);
                    spinnerAdapter.clear();
                    spinnerAdapter.addAll(leagues);
                }

            }
        }, new TypeReference<ErrorModel>() {
        }));
        leaguesSpinner.post(new Runnable() {
            @Override
            public void run() {
                leaguesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        if (spinnerAdapter != null) {
                            League league = spinnerAdapter.getItem(position);
                            leagueId = league == null ? null : league.getId();
                            getEventsList();
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                    }
                });
            }
        });
    }
}
