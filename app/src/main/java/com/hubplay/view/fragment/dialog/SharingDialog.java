package com.hubplay.view.fragment.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.hubplay.R;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * 25.07.16.
 *
 * @author Alexey Vereshchaga
 */
public class SharingDialog extends DialogFragment implements View.OnClickListener {

    private EditText etDescription;

    public static SharingDialog newInstance() {
        SharingDialog dialog = new SharingDialog();
//        Bundle args = new Bundle();
//        args.putString(DIALOG_TYPE, dialogType.name());
//        dialog.setArguments(args);
        return dialog;
    }

    @NonNull
    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),
                android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar);
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View v = inflater.inflate(R.layout.dialog_share, null);
        etDescription = (EditText) v.findViewById(R.id.et_text);
        v.findViewById(R.id.btn_share).setOnClickListener(this);
        builder.setView(v);
        return builder.create();
    }

    @Override
    public void onResume() {
        super.onResume();
        getDialog().setCanceledOnTouchOutside(true);
        getDialog().setCancelable(true);
        getDialog().getWindow().setBackgroundDrawableResource(R.drawable.dialog_bg);
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onClick(View v) {
//        String tweetUrl = String.format(getResources().getString(R.string.sharing_twitter_url),
//                urlEncode(etDescription.getText().toString()),
//                urlEncode(getResources().getString(R.string.hub_play_link)));
//        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(tweetUrl));
//        List<ResolveInfo> matches = getActivity().getPackageManager().queryIntentActivities(intent, 0);
//        for (ResolveInfo info : matches) {
//            if (info.activityInfo.packageName.toLowerCase().startsWith("com.twitter")) {
//                intent.setPackage(info.activityInfo.packageName);
//            }
//        }
//        startActivity(intent);
        dismiss();
    }

    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.wtf(SharingDialog.class.getSimpleName(), "UTF-8 should always be supported", e);
            throw new RuntimeException("URLEncoder.encode() failed for " + s);
        }
    }

}