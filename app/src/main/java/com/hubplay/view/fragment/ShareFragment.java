package com.hubplay.view.fragment;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.facebook.CallbackManager;
import com.facebook.login.LoginManager;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.hubplay.R;
import com.hubplay.controller.ToolbarController;
import com.hubplay.network.model.response.Event;
import com.hubplay.utils.Constant;
import com.hubplay.utils.HelpUtils;
import com.hubplay.utils.StoryType;
import com.hubplay.view.activity.MainActivity;
import com.hubplay.view.adapter.recycler.RecyclerViewFooterAdapter;
import com.hubplay.view.fragment.dialog.SharingDialog;
import com.hubplay.view.fragment.home.HomeFragment;
import com.hubplay.view.fragment.home.tab.EventsFragment;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 19.07.16.
 *
 * @author Alexey Vereshchaga
 */
public class ShareFragment extends AbstractFragment<MainActivity> {

    public static final String TAG = ShareFragment.class.getSimpleName();

    @BindView(R.id.ll_facebook)
    LinearLayout llFacebook;
    @BindView(R.id.ll_twitter)
    LinearLayout llTwitter;
    @BindView(R.id.tv_share)
    TextView tvShare;
    @BindView(R.id.iv_big_wheel)
    ImageView ivBigWheel;
    @BindView(R.id.iv_share_back)
    ImageView ivBackground;
    @BindView(R.id.iv_next_story)
    ImageView ivNextStory;

    //next story
    @BindView(R.id.ll_next_story)
    LinearLayout llNextStory;

    private Event event;
    private Event nextEvent;

    private CallbackManager callbackManager;
    ShareDialog shareDialog;

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_share;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        event = HelpUtils.readStringToObject(getArguments().getString(Constant.EVENT_KEY), Event.class);
        StoryType storyType = null;

        EventsFragment tabFragment = null;
        if (event != null) {
            storyType = StoryType.valueOf(StoryType.class, event.getCategory());
            Log.d(TAG, "onCreate: storyType: " + storyType);
        }
        HomeFragment homeFragment = (HomeFragment) getActivity().getSupportFragmentManager()
                .findFragmentByTag(HomeFragment.class.getName());

        if (homeFragment != null && storyType != null) {
            tabFragment = (EventsFragment) homeFragment.getPagerAdapter().getItem(storyType);
            Log.d(TAG, "onCreate: homeFragment: " + homeFragment);
            Log.d(TAG, "onCreate: tabFragment: " + tabFragment);
        }
        if (tabFragment != null) {
            RecyclerViewFooterAdapter recyclerAdapter = tabFragment.getRecyclerAdapter();
            if (recyclerAdapter != null) {
                List<Event> events = recyclerAdapter.getDataList();
                Integer index = events.indexOf(event);
                if (index < events.size() - 1) {
                    nextEvent = events.get(index + 1);
                    Log.d(TAG, "onCreate: nextEvent: " + nextEvent);
                }
            }
        }
    }

    @Override
    protected void initView(View view) {
        RequestManager requestManager = Glide.with(this);
        requestManager
                .load(event.getBgIcon())
                .centerCrop()
                .into(ivBackground);
        if (nextEvent != null) {
            requestManager
                    .load(nextEvent.getBgIcon())
                    .centerCrop()
                    .into(ivNextStory);
        } else {
            llNextStory.setVisibility(View.GONE);
        }

        int measureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        llFacebook.measure(measureSpec, measureSpec);
        tvShare.measure(measureSpec, measureSpec);

        int measuredHeight = llFacebook.getMeasuredHeight();
        int measuredWidthTrivia = llFacebook.getMeasuredWidth();
        int measuredWidthChooseTool = tvShare.getMeasuredWidth();

        ivBigWheel.getLayoutParams().height = measuredHeight + getResources().getDimensionPixelOffset(R.dimen.choose_tool_top_button_margin);
        ivBigWheel.getLayoutParams().width = 2 * measuredWidthTrivia
                + measuredWidthChooseTool
                + 2 * getResources().getDimensionPixelOffset(R.dimen.choose_tool_button_size);
    }

    @Override
    public void onResume() {
        super.onResume();
        callbackManager = CallbackManager.Factory.create();
        ToolbarController toolbarController = getBaseActivity().getToolbarController();
        toolbarController.setTitle(getResources().getString(R.string.share_title));
        Integer points = getBaseActivity().getUser().getPointBalance();
        String pointsStr = String.format(Locale.US, getResources().getString(R.string.header_hub_points), points);
        toolbarController.setRightText(pointsStr);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @OnClick(R.id.btn_done)
    void onClickDone() {
        getActivity().onBackPressed();
    }

    @OnClick(R.id.ll_email)
    void onClickEmail() {
        //// TODO: 25.07.16 need subject and text
        Intent i = new Intent(Intent.ACTION_SENDTO);
        i.setType("text/plain");
        i.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.share_trivia_hub_play));
        i.putExtra(Intent.EXTRA_TEXT, getResources().getString(R.string.hub_play_link));
        i.setData(Uri.parse("mailto:"));
        try {
            startActivity(Intent.createChooser(i, getResources().getString(R.string.share_a_few_words)));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(getContext(), "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    @OnClick(R.id.ll_facebook)
    void onClickFacebook() {
        LoginManager.getInstance().logInWithPublishPermissions(this, Arrays.asList("publish_actions"));
        shareDialog = new ShareDialog(this);
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentUrl(Uri.parse(getResources().getString(R.string.hub_play_link)))
                    .build();
            shareDialog.show(linkContent);
        }
    }

    @OnClick(R.id.ll_twitter)
    void onClickTwitter() {
//        SharingDialog shareDialog = SharingDialog.newInstance();
//        shareDialog.show(getFragmentManager(), "2");
        String tweetUrl = String.format(getResources().getString(R.string.sharing_twitter_url),
                urlEncode(getResources().getString(R.string.hub_play_link)));
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(tweetUrl));

        List<ResolveInfo> matches = getActivity().getPackageManager().queryIntentActivities(intent, 0);
        for (ResolveInfo info : matches) {
            if (info.activityInfo.packageName.toLowerCase().startsWith("com.twitter")) {
                intent.setPackage(info.activityInfo.packageName);
            }
        }
        startActivity(intent);
    }

    public static String urlEncode(String s) {
        try {
            return URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            Log.wtf(SharingDialog.class.getSimpleName(), "UTF-8 should always be supported", e);
            throw new RuntimeException("URLEncoder.encode() failed for " + s);
        }
    }
}
