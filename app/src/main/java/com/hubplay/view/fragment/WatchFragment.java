package com.hubplay.view.fragment;

import android.animation.ObjectAnimator;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.hubplay.R;
import com.hubplay.controller.Screen;
import com.hubplay.controller.TransitionsManager;
import com.hubplay.network.model.MediaType;
import com.hubplay.network.model.response.Event;
import com.hubplay.network.model.response.Watch;
import com.hubplay.utils.Constant;
import com.hubplay.utils.HelpUtils;
import com.hubplay.utils.YouTubeExtractor;
import com.hubplay.view.activity.MainActivity;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * 27.07.16.
 *
 * @author Alexey Vereshchaga
 */
public class WatchFragment extends AbstractFragment<MainActivity> {
    public static final int DEFAULT_DURATION = 5;
    //main part
    @BindView(R.id.iv_contest_main)
    ImageView ivContestMain;
    @BindView(R.id.vv_contest_main)
    VideoView vvContestMain;
    //header
    @BindView(R.id.tv_question_number)
    TextView tvWatchNumber;
    @BindView(R.id.tv_description)
    TextView tvTitle;
    //seek bar
    @BindView(R.id.sb_linear)
    SeekBar sbLinear;
    @BindView(R.id.tv_seek_bar_center)
    TextView tvSeekBarCenter;
    @BindView(R.id.tv_seek_bar_end)
    TextView tvSeekBarEnd;
    @BindView(R.id.tv_skip)
    TextView tvSkip;
    //progress bar
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindViews({R.id.iv_contest_main, R.id.vv_contest_main})
    List<View> backgroundViews;

    private Event event;
    private ObjectAnimator preMediaAnimation;
    private Handler preMediaHandler;
    private Handler progressBarHandler = new Handler();
    private Boolean isPreMediaFinished = false;
    private int watchIndex = -1;

    private WatchListener listener = new WatchListener() {
        @Override
        public void onPreMediaFinish() {
            if (isPreMediaFinished) {
                watchIndex++;
                startWatch(watchIndex);
            }
        }

        @Override
        public void onNextWatch() {
            tvSkip.setVisibility(View.VISIBLE);
            ButterKnife.apply(backgroundViews, SHOW_IMAGE, false);
            watchIndex++;
            startWatch(watchIndex);
        }
    };

    private static final ButterKnife.Setter<View, Boolean> SHOW_IMAGE = new ButterKnife.Setter<View, Boolean>() {
        @Override
        public void set(@NonNull View view, Boolean value, int index) {
            switch (view.getId()) {
                case R.id.iv_contest_main:
                    view.setVisibility(value ? View.VISIBLE : View.GONE);
                    break;
                case R.id.vv_contest_main:
                    view.setVisibility(value ? View.GONE : View.VISIBLE);
                    break;
            }
        }
    };

    private Runnable run = new Runnable() {
        @Override
        public void run() {
            progressBarUpdate();
        }
    };

    //// TODO: 28.07.16 need refactoring, min  - common part to superclass

    public void progressBarUpdate() {
        if (vvContestMain != null) {
            if (vvContestMain.getDuration() >= vvContestMain.getCurrentPosition()) {
                sbLinear.setProgress(vvContestMain.getCurrentPosition());
                progressBarHandler.postDelayed(run, 100);
            } else {
                sbLinear.setProgress(vvContestMain.getDuration());
                progressBarHandler.removeCallbacksAndMessages(null);
            }
        }
    }

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_watch;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        event = HelpUtils.readStringToObject(getArguments().getString(Constant.EVENT_KEY), Event.class);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        if (!isPreMediaFinished) {
            startPreMedia();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        removePreMediaHandler();
    }

    private void removePreMediaHandler() {
        if (preMediaHandler != null) {
            preMediaHandler.removeCallbacksAndMessages(null);
            preMediaHandler = null;
        }
    }

    private void startPreMedia() {
        if (event.getPreMedia() != null) {
            tvTitle.setText(R.string.watch_intro_video);
            MediaType mediaType = HelpUtils.getEnumByName(MediaType.class, event.getPreMedia().getMediaType());
            startPreMediaByType(mediaType);
        } else {
            isPreMediaFinished = true;
            listener.onPreMediaFinish();
        }
    }

    /**
     * In pre media in different ways moves to the the next step if the video duration is not setted
     *
     * @param mediaType
     */
    private void startPreMediaByType(MediaType mediaType) {
        if (mediaType != null) {
            String url = event.getPreMedia().getUrl();
            Integer preMediaDuration = event.getPreMedia().getDuration();
            if (preMediaDuration != null) {
                startPreMedia(mediaType, url, preMediaDuration);
            } else if (mediaType == MediaType.GIF || mediaType == MediaType.IMAGE) {
                preMediaDuration = DEFAULT_DURATION;
                startPreMedia(mediaType, url, preMediaDuration);
            } else {
                setContentByType(mediaType, url, true);
            }
        }
    }

    private void startPreMedia(MediaType mediaType, String url, Integer preMediaDuration) {
        createPreMediaHandler(preMediaDuration * DateUtils.SECOND_IN_MILLIS);
        startPreMediaProgressBar(preMediaDuration);
        setContentByType(mediaType, url);
    }

    private void startPreMediaProgressBar(int durationInSeconds) {
        int durationInMillis = (int) (durationInSeconds * DateUtils.SECOND_IN_MILLIS);
        setSeekBarValues(durationInSeconds, durationInMillis);
        preMediaAnimation = ObjectAnimator.ofInt(sbLinear, "progress", durationInMillis);
        preMediaAnimation.setDuration(durationInMillis);
        preMediaAnimation.setInterpolator(null);
        preMediaAnimation.start();
    }

    private void createPreMediaHandler(long millis) {
        removePreMediaHandler();
        preMediaHandler = new Handler();
        preMediaHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                isPreMediaFinished = true;
                listener.onPreMediaFinish();
            }
        }, millis);
    }

    private void setContentByType(MediaType mediaType, String url) {
        setContentByType(mediaType, url, false);
    }

    private void setContentByType(MediaType mediaType, String url, final boolean isVideoWithoutDuration) {
        switch (mediaType) {
            case YOUTUBE:
                Uri uri = Uri.parse(url);
                String identifier = uri.getQueryParameter("v");
                YouTubeExtractor extractor = new YouTubeExtractor(identifier);
                extractor.startExtracting(new YouTubeExtractor.YouTubeExtractorListener() {
                    @Override
                    public void onSuccess(YouTubeExtractor.YouTubeExtractorResult result) {
                        startVideo(result.getVideoUri().toString(), isVideoWithoutDuration);
                    }

                    @Override
                    public void onFailure(Error error) {

                    }
                });
                break;
            case MP4:
                ButterKnife.apply(backgroundViews, SHOW_IMAGE, false);
                startVideo(url, isVideoWithoutDuration);
                break;
            case GIF:
            case IMAGE:
                ButterKnife.apply(backgroundViews, SHOW_IMAGE, true);
                if (this.isAdded()) {
                    Glide
                            .with(this)
                            .load(url)
                            .into(ivContestMain);
                }
                break;
            case NONE:
                ButterKnife.apply(backgroundViews, SHOW_IMAGE, false);
                startVideo(url, true);
                break;
        }
    }

    private void startVideo(String url, final boolean isVideoWithoutDuration) {
        try {
            Uri uri = Uri.parse(url);
            vvContestMain.stopPlayback();
            vvContestMain.setVideoURI(uri);
            vvContestMain.requestFocus();
            vvContestMain.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    Toast.makeText(getActivity(), "Video error", Toast.LENGTH_SHORT).show();
                    return true;
                }
            });
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                vvContestMain.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                    @Override
                    public boolean onInfo(MediaPlayer mp, int what, int extra) {
                        if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                            vvContestMain.setBackgroundColor(Color.TRANSPARENT);
                            if (isVideoWithoutDuration) {
                                startProgressByVideo();
                            }
//                            return true;
                        }
                        progressBar.setVisibility(what == MediaPlayer.MEDIA_INFO_BUFFERING_START ? View.VISIBLE : View.GONE);
                        return false;
                    }
                });
            } else {
                vvContestMain.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        vvContestMain.setBackgroundColor(Color.TRANSPARENT);
                        if (isVideoWithoutDuration) {
                            startProgressByVideo();
                        }
                    }
                });
            }
            vvContestMain.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    if (!isPreMediaFinished) {
                        isPreMediaFinished = true;
                        listener.onPreMediaFinish();
                    } else {
                        listener.onNextWatch();
                    }
                }
            });
            vvContestMain.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startProgressByVideo() {
        setSeekBarValues(Math.round(vvContestMain.getDuration() / 1000), vvContestMain.getDuration());
        progressBarUpdate();
    }

    private void setSeekBarValues(int lastSecond) {
        tvSeekBarEnd.setText(":" + lastSecond);
        DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols(Locale.US);
        formatSymbols.setDecimalSeparator('.');
        String formatted = new DecimalFormat("#.#", formatSymbols).format((float) lastSecond / 2);
        tvSeekBarCenter.setText(":" + formatted);
    }

    private void setSeekBarValues(int durationInSeconds, int durationInMillis) {
        sbLinear.setMax(durationInMillis);
        setSeekBarValues(durationInSeconds);
    }

    private void startWatch(int watchIndex) {
        clearSeekBar();
        if (watchIndex <= event.getWatches().size() - 1) {
            Watch watch = event.getWatches().get(watchIndex);
            tvTitle.setText(watch.getTitle());
            tvWatchNumber.setText(String.format(Locale.US, getResources().getString(R.string.watch_number), watchIndex + 1, event.getWatches().size()));
            setContentByType(MediaType.NONE, watch.getUrl(), true);
        } else {
            Bundle bundle = new Bundle();
            String eventStr = HelpUtils.createStringFromObject(event);
            bundle.putString(Constant.EVENT_KEY, eventStr);
            TransitionsManager.getInstance().popBackStackToFirst();
            TransitionsManager.getInstance().replaceFragment(Screen.SHARE, bundle);
        }
    }

    private void clearSeekBar() {
        removePreMediaHandler();
        if (preMediaAnimation != null) {
            preMediaAnimation.cancel();
            preMediaAnimation = null;
        }
        sbLinear.clearAnimation();
        sbLinear.setProgress(0);
        tvSeekBarCenter.setText("");
        tvSeekBarEnd.setText("");
    }

    @OnClick(R.id.tv_skip)
    void onSkip() {
        if (!isPreMediaFinished) {
            isPreMediaFinished = true;
            listener.onPreMediaFinish();
        } else {
            listener.onNextWatch();
        }
    }

    private interface WatchListener {
        void onPreMediaFinish();

        void onNextWatch();
    }

}
