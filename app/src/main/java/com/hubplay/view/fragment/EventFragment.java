package com.hubplay.view.fragment;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.format.DateUtils;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.fasterxml.jackson.core.type.TypeReference;
import com.hubplay.R;
import com.hubplay.TheApplication;
import com.hubplay.controller.DataManager;
import com.hubplay.controller.Screen;
import com.hubplay.controller.TransitionsManager;
import com.hubplay.network.BaseHandler;
import com.hubplay.network.BaseRemoteListener;
import com.hubplay.network.WrapperCall;
import com.hubplay.network.model.Action;
import com.hubplay.network.model.AnswerType;
import com.hubplay.network.model.ErrorModel;
import com.hubplay.network.model.MediaType;
import com.hubplay.network.model.response.Answer;
import com.hubplay.network.model.response.Event;
import com.hubplay.network.model.response.Media;
import com.hubplay.network.model.response.Question;
import com.hubplay.network.model.response.QuestionOrder;
import com.hubplay.utils.Constant;
import com.hubplay.utils.HelpUtils;
import com.hubplay.utils.SharedPreferenceHelper;
import com.hubplay.utils.YouTubeExtractor;
import com.hubplay.view.activity.MainActivity;
import com.hubplay.view.fragment.dialog.EventDialog;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;

/**
 * 09.06.16.
 *
 * @author Alexey Vereshchaga
 */
public class EventFragment extends AbstractFragment<MainActivity> {

    private static final String TAG = EventFragment.class.getSimpleName();
    public static final int DEFAULT_DURATION = 5;

    @BindView(R.id.rl_event)
    RelativeLayout rlEvent;
    @BindView(R.id.iv_contest_main)
    ImageView ivContestMain;
    @BindView(R.id.vv_contest_main)
    VideoView vvContestMain;
    //header
    @BindView(R.id.tv_question_number)
    TextView tvQuestionNumber;
    @BindView(R.id.tv_description)
    TextView tvDescription;
    @BindView(R.id.tv_points)
    TextView tvPoints;
    //teams
    @BindView(R.id.ll_teams)
    LinearLayout llTeams;
    @BindView(R.id.iv_first_team)
    ImageView ivFirstTeam;
    @BindView(R.id.iv_second_team)
    ImageView ivSecondTeam;
    //seek bar
    @BindView(R.id.ll_pre_media_seek_bar)
    LinearLayout llPreMediaProgress;
    @BindView(R.id.sb_linear)
    SeekBar sbLinear;
    @BindView(R.id.tv_seek_bar_center)
    TextView tvSeekBarCenter;
    @BindView(R.id.tv_seek_bar_end)
    TextView tvSeekBarEnd;
    @BindView(R.id.tv_skip)
    TextView tvSkip;
    // question
    @BindView(R.id.ll_question)
    LinearLayout llQuestion;
    @BindView(R.id.pbCircular)
    ProgressBar pbCircular;
    @BindView(R.id.tv_countdown)
    TextView tvQuestionCountdown;
    @BindView(R.id.tv_question)
    TextView tvQuestion;
    @BindView(R.id.ll_answers_variants)
    RelativeLayout rlAnswers;
    @BindView(R.id.iv_answer_variant_icon)
    ImageView ivAnswerVariant;
    @BindView(R.id.ll_answer_1)
    LinearLayout llAnswer1;
    @BindView(R.id.ll_answer_2)
    LinearLayout llAnswer2;
    @BindView(R.id.ll_answer_3)
    LinearLayout llAnswer3;
    @BindView(R.id.ll_answer_4)
    LinearLayout llAnswer4;
    @BindView(R.id.tv_sponsor_by)
    TextView tvSponsorBy;
    @BindView(R.id.iv_sponsor)
    ImageView ivSponsor;
    //answer
    @BindView(R.id.iv_answer)
    ImageView ivAnswer;
    @BindView(R.id.tv_answer_points)
    TextView tvAnswerPoints;
    //post media
    @BindView(R.id.tv_event_share)
    TextView tvRedeem;

    @BindViews({R.id.iv_contest_main, R.id.vv_contest_main})
    List<View> backgroundViews;
    @BindViews({R.id.ll_answer_1, R.id.ll_answer_2, R.id.ll_answer_3, R.id.ll_answer_4})
    List<LinearLayout> answersList;

    //    private ContestPaymentType contestPaymentType;
    private Event event;
    private int currentQuestionIndex;
    private int currentPoints;

    private Handler preMediaHandler;
    private Handler postQuestionHandler;

    private Boolean isStarted = false;
    private Boolean isPreMediaFinished = false;
    private Boolean isAnswerSaved = false;
    private Boolean isPostQuestionFinished = false;
    // question
    private CountDownTimer countDownTimer;
    private int timePassed;
    private long mLastClickTime;
    private int[] letters = {R.drawable.ic_a, R.drawable.ic_b, R.drawable.ic_c, R.drawable.ic_d};

    private WrapperCall currentCall;
    private Handler progressBarHandler = new Handler();

    private Runnable run = new Runnable() {
        @Override
        public void run() {
            progressBarUpdate();
        }
    };

    public void progressBarUpdate() {
        if (vvContestMain != null) {
            if (vvContestMain.getDuration() >= vvContestMain.getCurrentPosition()) {
                sbLinear.setProgress(vvContestMain.getCurrentPosition());
                progressBarHandler.postDelayed(run, 100);
            } else {
                sbLinear.setProgress(vvContestMain.getDuration());
                progressBarHandler.removeCallbacks(run);
            }
        }
    }

    private static final ButterKnife.Setter<View, Boolean> SHOW_IMAGE = new ButterKnife.Setter<View, Boolean>() {
        @Override
        public void set(@NonNull View view, Boolean value, int index) {
            switch (view.getId()) {
                case R.id.iv_contest_main:
                    view.setVisibility(value ? View.VISIBLE : View.GONE);
                    break;
                case R.id.vv_contest_main:
                    view.setVisibility(value ? View.GONE : View.VISIBLE);
                    break;
            }
        }
    };

    DialogInterface.OnClickListener onClickListener = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            SharedPreferenceHelper.markEventDialogNotShown();
            switch (which) {
                case AlertDialog.BUTTON_POSITIVE:
                    getAwayWhenError();
                    break;
                case AlertDialog.BUTTON_NEGATIVE:
                    if (currentCall != null) {
                        Call clonedCall = currentCall.getCall().clone();
                        clonedCall.enqueue(currentCall.getHandler());
                    }
                    break;
            }
        }
    };

    //// TODO: 27.06.16 refactoring is needed
    private InternalEventListener internalEventListener = new InternalEventListener() {
        @Override
        public void onPreMediaFinish() {
            if (isStarted && isPreMediaFinished) {
                startQuestion();
            }
        }

        @Override
        public void onQuestionFinish(Answer answer) {
            startPostQuestion(event.getQuestion().get(currentQuestionIndex), answer);
            currentQuestionIndex++;
            cacheQuestionImages(currentQuestionIndex);
        }

        @Override
        public void onPostQuestionFinish() {
            if (isAnswerSaved && isPostQuestionFinished) {
                isAnswerSaved = false;
                isPostQuestionFinished = false;
                startQuestion();
            }
        }

        @Override
        public void onAllQuestionsFinish() {
            if (isAdded()) {
                startPostMedia();
            }
        }

        @Override
        public void onPostMediaFinish() {
        }
    };

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_event;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        contestPaymentType = ContestPaymentType.valueOf(getArguments().getString(CONTEST_PAYMENT_TYPE_KEY));
        String eventStr = getArguments().getString(Constant.EVENT_KEY);
        event = HelpUtils.readStringToObject(eventStr, Event.class);
        if (event != null) {
            currentCall = DataManager.sendAction(event.getId(), Action.STARTED, new BaseHandler<>(
                    new BaseEventListener<QuestionOrder>(getActivity()) {
                        @Override
                        public void onSuccess(QuestionOrder result) {
                            if (result != null) {
                                if (result.getNextQuestionOrder() != null) {
                                    currentQuestionIndex = result.getNextQuestionOrder();
                                }
                                if (result.getPoints() != null) {
                                    currentPoints = result.getPoints();
                                }
                            }
                            isStarted = true;
                            internalEventListener.onPreMediaFinish();
                        }
                    }, new TypeReference<ErrorModel>() {
            }));
        }
    }

    @Override
    protected void initView(View view) {
        sbLinear.setEnabled(false);
        if (event != null) {
            setTeamsImages();
            startPreMedia();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    //pre media
    private void startPreMedia() {
        if (event.getPreMedia() != null) {
            llPreMediaProgress.setVisibility(View.VISIBLE);
            tvSkip.setVisibility(View.VISIBLE);
            tvDescription.setText(R.string.event_pregame);
            String eventPoints = String.format(Locale.US,
                    getResources().getString(R.string.event_header_hub_points),
                    HelpUtils.getEventPoints(event.getQuestion()));
            tvPoints.setText(eventPoints);
            MediaType mediaType = HelpUtils.getEnumByName(MediaType.class, event.getPreMedia().getMediaType());
            startPreMediaByType(mediaType);
            cacheQuestionImages(currentQuestionIndex);
        } else {
            isPreMediaFinished = true;
            internalEventListener.onPreMediaFinish();
        }
    }

    /**
     * In pre media in different ways moves to the the next step if the video duration is not setted
     *
     * @param mediaType
     */
    private void startPreMediaByType(MediaType mediaType) {
        if (mediaType != null) {
            String url = event.getPreMedia().getUrl();
            Integer preMediaDuration = event.getPreMedia().getDuration();
            if (preMediaDuration != null) {
                startPreMedia(mediaType, url, preMediaDuration);
            } else if (mediaType == MediaType.GIF || mediaType == MediaType.IMAGE) {
                preMediaDuration = DEFAULT_DURATION;
                startPreMedia(mediaType, url, preMediaDuration);
            } else {
                setContentByType(mediaType, url, true);
            }
        }
    }

    private void startPreMedia(MediaType mediaType, String url, Integer preMediaDuration) {
        createPreMediaHandler(preMediaDuration * DateUtils.SECOND_IN_MILLIS);
        startPreMediaProgressBar(preMediaDuration);
        setContentByType(mediaType, url);
    }

    private void createPreMediaHandler(long millis) {
        if (preMediaHandler != null) {
            preMediaHandler.removeCallbacksAndMessages(null);
        }
        preMediaHandler = new Handler();
        preMediaHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (internalEventListener != null) {
                    isPreMediaFinished = true;
                    internalEventListener.onPreMediaFinish();
                }
            }
        }, millis);
    }

    private void startPreMediaProgressBar(int durationInSeconds) {
        int durationInMillis = (int) (durationInSeconds * DateUtils.SECOND_IN_MILLIS);
        setSeekBarValues(durationInSeconds, durationInMillis);
        ObjectAnimator preMediaAnimation = ObjectAnimator.ofInt(sbLinear, "progress", durationInMillis);
        preMediaAnimation.setDuration(durationInMillis);
        preMediaAnimation.setInterpolator(null);
        preMediaAnimation.start();
    }

    private void setSeekBarValues(int durationInSeconds, int durationInMillis) {
        sbLinear.setMax(durationInMillis);
        setSeekBarValues(durationInSeconds);
    }

    private void startQuestion() {
        setQuestionViewState();
        List<Question> questions = event.getQuestion();
        if (this.isAdded() && questions != null
                && questions.size() > 0
                && questions.size() >= currentQuestionIndex + 1) {
            Question question = questions.get(currentQuestionIndex);
            Resources res = TheApplication.getInstance().getResources();
            tvQuestionNumber.setText(String.format(Locale.US, res.getString(R.string.event_number_of_question), currentQuestionIndex + 1, questions.size()));
            tvDescription.setText(Html.fromHtml("<i>" + question.getTitle() + "</i>"));
            tvPoints.setText(String.format(Locale.US, getResources().getString(R.string.event_header_hub_points), question.getMaxPoint()));
            tvQuestion.setText(question.getText());
            if (question.getSponsor() != null && !TextUtils.isEmpty(question.getSponsor().getIcon())) {
                ivSponsor.setVisibility(View.VISIBLE);
                Glide
                        .with(this)
                        .load(question.getSponsor().getIcon())
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .dontAnimate()
                        .into(ivSponsor);
                tvSponsorBy.setText(getResources().getString(R.string.sponsor_by));
            } else {
                tvSponsorBy.setVisibility(View.GONE);
                ivSponsor.setVisibility(View.GONE);
            }

            setAnswersOnClickListeners();
            if (questions.get(currentQuestionIndex).getMedia() != null) {
                Media media = question.getMedia();
                if (media != null) {
                    setContentByType(HelpUtils.getEnumByName(MediaType.class, media.getMediaType()), media.getUrl());
                }
            }
            setAnswers(question.getAnswers(), question.getAnswerType());
            if (question.getIcon() != null) {
                ivAnswerVariant.setVisibility(View.VISIBLE);
                Glide
                        .with(this)
                        .load(question.getIcon())
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .dontAnimate()
                        .into(ivAnswerVariant);
            } else {
                ivAnswerVariant.setVisibility(View.GONE);
            }

            createTimer((int) (question.getTimer() * DateUtils.SECOND_IN_MILLIS));
        } else {
            internalEventListener.onAllQuestionsFinish();
        }
    }

    private void setAnswers(List<Answer> answers, String answerTypeStr) {
        int answersSize = answers.size();
        AnswerType answerType = HelpUtils.getEnumByName(AnswerType.class, answerTypeStr);
        int answerIndex = 0;
        for (LinearLayout llAnswer :
                answersList) {
            ImageView ivAnswer = (ImageView) llAnswer.findViewById(R.id.iv_answer_letter);
            TextView tvAnswer = (TextView) llAnswer.findViewById(R.id.tv_answer);
            if (answerType != null && answersSize - 1 >= answerIndex) {
                switch (answerType) {
                    case ICON:
                        Glide.with(this)
                                .load(answers.get(answerIndex).getIcon())
                                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                                .dontAnimate()
                                .into(ivAnswer);
                        break;
                    case LETTER:
                        ivAnswer.setImageResource(letters[answerIndex]);
                        break;
                    case TEXT:
                        ivAnswer.setImageDrawable(null);
                        break;
                }
                tvAnswer.setText(answers.get(answerIndex).getText());
                answerIndex++;
            }
        }
    }

    private void createTimer(int millis) {
        int countDownInterval = 1000;
        Resources res = TheApplication.getInstance().getResources();
        final String timerStr = res.getString(R.string.event_question_progress);
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
        countDownTimer = new CountDownTimer(millis, countDownInterval) {
            @Override
            public void onTick(long millisUntilFinished) {
                setCountdownValue((int) (millisUntilFinished / DateUtils.SECOND_IN_MILLIS));
                timePassed++;
            }

            @Override
            public void onFinish() {
                int defaultWrongAnswerId = -1;
                setCountdownValue(0);
                Answer answer = new Answer();
                answer.setId(defaultWrongAnswerId);
                answer.setCorrect(false);
                internalEventListener.onQuestionFinish(answer);
                timePassed = 0;
            }

            private void setCountdownValue(int value) {
                if (tvQuestionCountdown != null) {
                    String time = String.format(timerStr, value);
                    tvQuestionCountdown.setText(time);
                }
            }
        };
        countDownTimer.start();
        startCircularCountdown(millis);
    }

    private void startPostQuestion(Question question, @Nullable Answer answer) {
        if (getActivity() != null && answer != null) {
            countDownTimer.cancel();
            removeAnswerOnClickListeners();
            Media postQuestionMedia = answer.getCorrect() ? question.getWinMedia() : question.getLoseMedia();
            if (answer.getCorrect()) {
                ((Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE)).vibrate(800);
            }
            int points = HelpUtils.getPoints(question.getMaxPoint(),
                    question.getMinPoint(),
                    question.getLosePoint(),
                    timePassed,
                    answer.getCorrect());
            currentPoints += points;
            saveAnswer(question, answer, points);
            if (vvContestMain.isPlaying()) {
                vvContestMain.stopPlayback();
            }
            timePassed = 0;
            setPostQuestionViews();
            buildPostQuestionView(points, answer);
            if (postQuestionMedia != null) {
                createPostQuestionHandler(postQuestionMedia.getDuration());
                setContentByType(HelpUtils.getEnumByName(MediaType.class, postQuestionMedia.getMediaType()), postQuestionMedia.getUrl());
            } else {
                createPostQuestionHandler(null);
            }
            if (!answer.getCorrect()) {
                startWrongAnswerAnimation();
            }
        }
    }

    private void saveAnswer(Question question, Answer answer, int points) {
        Boolean isLast = event.getQuestion().size() == currentQuestionIndex + 1 ? true : null;
        currentCall = DataManager.saveAnswer(answer.getId(),
                points,
                question.getId(),
                event.getId(),
                isLast,
                new BaseHandler<>(new BaseEventListener<Void>(getActivity()) {
                    @Override
                    public void onSuccess(Void result) {
                        isAnswerSaved = true;
                        internalEventListener.onPostQuestionFinish();
                    }
                }, new TypeReference<ErrorModel>() {
                }));
    }

    private void startWrongAnswerAnimation() {
        int duration = 50;
        ObjectAnimator animator = ObjectAnimator
                .ofFloat(rlEvent, "translationX", 0, 25, -25, 0)
                .setDuration(duration);
        animator.setRepeatCount(10);
        animator.setInterpolator(null);
        animator.start();
    }

    private void removeAnswerOnClickListeners() {
        for (LinearLayout llAnswer :
                answersList) {
            llAnswer.setOnClickListener(null);
        }
    }

    private void createPostQuestionHandler(@Nullable Integer postQuestionMediaDuration) {
        if (postQuestionMediaDuration == null) {
            postQuestionMediaDuration = DEFAULT_DURATION;
        }
        if (postQuestionHandler != null) {
            postQuestionHandler.removeCallbacksAndMessages(null);
        }
        postQuestionHandler = new Handler();
        postQuestionHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                isPostQuestionFinished = true;
                internalEventListener.onPostQuestionFinish();
            }
        }, postQuestionMediaDuration * DateUtils.SECOND_IN_MILLIS);
    }

    private void buildPostQuestionView(int points, Answer answer) {
        Boolean correct = answer.getCorrect();
        String baseStr = TheApplication.getInstance().getString(R.string.event_answer_points);
        String pointsStr = String.format(Locale.US, baseStr, points);
        int colorRes = correct ? R.color.event_answer_correct : R.color.event_answer_wrong;
        int start = 3;
        int endOfNumber = pointsStr.indexOf(baseStr.substring(start));
        SpannableString spannableString = new SpannableString(pointsStr);
        Resources res = TheApplication.getInstance().getResources();
        spannableString.setSpan(new ForegroundColorSpan(res.getColor(colorRes)), 0, endOfNumber, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvAnswerPoints.setText(spannableString);
        tvQuestion.setText(correct ? R.string.event_answer_correct : R.string.event_answer_wrong);
        ivAnswer.setBackgroundResource(correct ? R.drawable.event_answer_correct_bg : R.drawable.event_answer_wrong_bg);
        ivAnswer.setImageResource(correct ? R.drawable.ic_correct : R.drawable.ic_wrong);
    }

    private void startPostMedia() {
        setPostMediaViews();
        tvQuestion.setText(R.string.event_post_media_congratulations);
        tvQuestionNumber.setText(R.string.event_post_media_congratulations_small);
        tvDescription.setText("");
        tvPoints.setText("");
        String winStr = String.format(Locale.US, TheApplication.getInstance().getString(R.string.event_post_media_you_win), currentPoints);
        SpannableString ss = new SpannableString(winStr);
        ss.setSpan(new RelativeSizeSpan(2f), winStr.indexOf("\n"), winStr.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvAnswerPoints.setText(ss);
        if (event.getPostMedia() != null) {
            Media postMedia = event.getPostMedia();
            if (postMedia != null) {
                setContentByType(HelpUtils.getEnumByName(MediaType.class, postMedia.getMediaType()), postMedia.getUrl());
            }
        }
    }

    private void setPostMediaViews() {
        if (isAdded()) {
            ivAnswer.setVisibility(View.GONE);
            tvQuestionCountdown.setVisibility(View.GONE);
            rlAnswers.setVisibility(View.GONE);
            pbCircular.setVisibility(View.GONE);
            tvAnswerPoints.setVisibility(View.VISIBLE);
            tvRedeem.setVisibility(View.VISIBLE);
        }
    }

    private void setPostQuestionViews() {
        if (isAdded()) {
            pbCircular.setVisibility(View.GONE);
            rlAnswers.setVisibility(View.GONE);
            tvQuestionCountdown.setVisibility(View.GONE);
            ivAnswer.setVisibility(View.VISIBLE);
            tvAnswerPoints.setVisibility(View.VISIBLE);
            ivSponsor.setVisibility(View.VISIBLE);
        }
    }

    private void setQuestionViewState() {
        if (isAdded()) {
            llPreMediaProgress.setVisibility(View.GONE);
            tvSkip.setVisibility(View.GONE);
            if (llQuestion.getVisibility() == View.INVISIBLE) {
                startQuestionAnimation();
            } else {
                llQuestion.setVisibility(View.VISIBLE);
            }
            rlAnswers.setVisibility(View.VISIBLE);
            ivAnswer.setVisibility(View.GONE);
            tvAnswerPoints.setVisibility(View.GONE);
            pbCircular.setVisibility(View.VISIBLE);
            tvQuestionCountdown.setVisibility(View.VISIBLE);
        }
    }

    private void startQuestionAnimation() {
        int duration = 300;
        ObjectAnimator animator = ObjectAnimator
                .ofFloat(llQuestion, "y", llQuestion.getHeight(), getResources().getDimension(R.dimen.event_header_height))
                .setDuration(duration);
        animator.setInterpolator(null);
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                llQuestion.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });
        animator.start();
    }

    private void getAwayWhenError() {
        if (getActivity() != null) {
            getActivity().onBackPressed();
        }
    }

    private void setTeamsImages() {
        if (event.getTeams() != null
                && event.getTeams().size() == 2) {
            llTeams.setVisibility(View.VISIBLE);
            RequestManager requestManager = Glide.with(this);
            requestManager
                    .load(event.getTeams().get(0).getIcon())
                    .dontAnimate()
                    .into(ivFirstTeam);
            requestManager
                    .load(event.getTeams().get(1).getIcon())
                    .dontAnimate()
                    .into(ivSecondTeam);
        } else {
            llTeams.setVisibility(View.GONE);
        }
    }

    private void setContentByType(MediaType mediaType, String url) {
        setContentByType(mediaType, url, false);
    }

    private void setContentByType(MediaType mediaType, String url, final boolean isVideoWithoutDuration) {
        switch (mediaType) {
            case YOUTUBE:
                Uri uri = Uri.parse(url);
                String identifier = uri.getQueryParameter("v");
                YouTubeExtractor extractor = new YouTubeExtractor(identifier);
                extractor.startExtracting(new YouTubeExtractor.YouTubeExtractorListener() {
                    @Override
                    public void onSuccess(YouTubeExtractor.YouTubeExtractorResult result) {
                        startVideo(result.getVideoUri().toString(), isVideoWithoutDuration);
                    }

                    @Override
                    public void onFailure(Error error) {

                    }
                });
                break;
            case MP4:
                ButterKnife.apply(backgroundViews, SHOW_IMAGE, false);
                startVideo(url, isVideoWithoutDuration);
                break;
            case GIF:
            case IMAGE:
                ButterKnife.apply(backgroundViews, SHOW_IMAGE, true);
                if (this.isAdded()) {
                    Glide
                            .with(this)
                            .load(url)
                            .into(ivContestMain);
                }
                break;
        }
    }

    private void setAnswersOnClickListeners() {
        for (LinearLayout llAnswer :
                answersList) {
            llAnswer.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                        return;
                    }
                    mLastClickTime = SystemClock.elapsedRealtime();
                    List<Answer> answers = null;
                    if (event != null && event.getQuestion().get(currentQuestionIndex) != null) {
                        answers = event.getQuestion().get(currentQuestionIndex).getAnswers();
                    }
                    int answerIndex = answersList.indexOf(v);
                    internalEventListener.onQuestionFinish(
                            answers != null && answers.size() >= answerIndex + 1
                                    ? answers.get(answerIndex)
                                    : null);
                }
            });
        }
    }


    private void startCircularCountdown(int millis) {
        pbCircular.setMax(millis);
        ObjectAnimator animation = ObjectAnimator.ofInt(pbCircular, "progress", 0, millis);
        animation.setDuration(millis); //in milliseconds
        animation.setInterpolator(null);
        animation.start();
    }


    private void startVideo(String url, final boolean isVideoWithoutDuration) {
        try {
            Uri uri = Uri.parse(url);
            vvContestMain.stopPlayback();
            vvContestMain.setVideoURI(uri);
            vvContestMain.requestFocus();
            vvContestMain.setOnErrorListener(new MediaPlayer.OnErrorListener() {
                @Override
                public boolean onError(MediaPlayer mp, int what, int extra) {
                    Toast.makeText(getActivity(), "Video error", Toast.LENGTH_SHORT).show();
                    return true;
                }
            });
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                vvContestMain.setOnInfoListener(new MediaPlayer.OnInfoListener() {
                    @Override
                    public boolean onInfo(MediaPlayer mp, int what, int extra) {
                        if (what == MediaPlayer.MEDIA_INFO_VIDEO_RENDERING_START) {
                            vvContestMain.setBackgroundColor(Color.TRANSPARENT);
                            if (isVideoWithoutDuration) {
                                startProgressByVideo();
                            }
                            return true;
                        }
                        return false;
                    }
                });
            } else {
                vvContestMain.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                    @Override
                    public void onPrepared(MediaPlayer mp) {
                        vvContestMain.setBackgroundColor(Color.TRANSPARENT);
                        if (isVideoWithoutDuration) {
                            startProgressByVideo();
                        }
                    }
                });
            }
            vvContestMain.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    if (internalEventListener != null) {
                        isPreMediaFinished = true;
                        internalEventListener.onPreMediaFinish();
                    }
                }
            });
            vvContestMain.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startProgressByVideo() {
        setSeekBarValues(Math.round(vvContestMain.getDuration() / 1000), vvContestMain.getDuration());
        progressBarUpdate();
    }

    private void setSeekBarValues(int lastSecond) {
        tvSeekBarEnd.setText(":" + lastSecond);
        DecimalFormatSymbols formatSymbols = new DecimalFormatSymbols(Locale.US);
        formatSymbols.setDecimalSeparator('.');
        String formatted = new DecimalFormat("#.#", formatSymbols).format((float) lastSecond / 2);
        tvSeekBarCenter.setText(":" + formatted);
    }

    private void cacheQuestionImages(Integer index) {
        if (event != null && event.getQuestion() != null && event.getQuestion().size() >= index + 1) {
            final Question question = event.getQuestion().get(index);
            if (question.getSponsor() != null && question.getSponsor().getIcon() != null) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        cacheIcon(question.getSponsor().getIcon());
                    }
                }).start();
            }
            if (question.getAnswers() != null && question.getAnswerType() != null) {
                List<Answer> answers = question.getAnswers();
                String answerTypeStr = question.getAnswerType();
                AnswerType answerType = HelpUtils.getEnumByName(AnswerType.class, answerTypeStr);
                if (answerType == AnswerType.ICON) {
                    for (final Answer answer :
                            answers) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                cacheIcon(answer.getIcon());
                            }
                        }).start();
                    }
                }
            }
            if (question.getIcon() != null) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        cacheIcon(question.getIcon());
                    }
                }).start();
            }
        }
    }

    private void cacheIcon(String url) {
        if (EventFragment.this.isAdded()) {
            Glide
                    .with(EventFragment.this)
                    .load(url)
                    .downloadOnly(-1, -1);
        }
    }

    @OnClick(R.id.tv_event_share)
    protected void clickShare() {
        Bundle bundle = new Bundle();
        String eventStr = HelpUtils.createStringFromObject(event);
        bundle.putString(Constant.EVENT_KEY, eventStr);
        TransitionsManager.getInstance().popBackStackToFirst();
        TransitionsManager.getInstance().replaceFragment(Screen.SHARE, bundle);
    }

    @OnClick(R.id.tv_skip)
    

    public DialogInterface.OnClickListener getOnClickListener() {
        return onClickListener;
    }

    private abstract class BaseEventListener<T> extends BaseRemoteListener<T, ErrorModel> {
        public static final String DIALOG_TAG = "1";

        public BaseEventListener(FragmentActivity activity) {
            super(activity);
        }

        @Override
        public void onFailure(ErrorModel errorModel) {
            if (activity != null && errorModel != null && errorModel.getError().equals("answer_already_exist")) {
                EventDialog eventDialog = EventDialog.newInstance(
                        activity.getString(R.string.event_dialog_title),
                        activity.getString(R.string.event_dialog_answer_already_exist),
                        EventDialog.DialogType.OK,
                        onClickListener);
                eventDialog.show(EventFragment.this.getFragmentManager(), DIALOG_TAG);
            }
        }

        @Override
        public void onError(Throwable t) {
            if (activity != null) {
                EventDialog eventDialog = EventDialog.newInstance(
                        activity.getString(R.string.event_dialog_title),
                        activity.getString(R.string.event_dialog_error_communication_with_server),
                        EventDialog.DialogType.RETRY,
                        onClickListener);
                eventDialog.show(EventFragment.this.getFragmentManager(), DIALOG_TAG);
            }
        }
    }

    private interface InternalEventListener {

        void onPreMediaFinish();

        void onQuestionFinish(Answer answer);

        void onPostQuestionFinish();

        void onAllQuestionsFinish();

        void onPostMediaFinish();
    }
}
