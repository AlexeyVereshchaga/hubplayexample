package com.hubplay.view.fragment;

import android.content.pm.ActivityInfo;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.hubplay.R;
import com.hubplay.controller.Screen;
import com.hubplay.controller.TransitionsManager;
import com.hubplay.network.model.response.User;
import com.hubplay.view.activity.MainActivity;
import com.hubplay.view.adapter.AccountAdapter;

import java.util.Locale;

import butterknife.BindView;

/**
 * 22.04.16.
 *
 * @author Alexey Vereshchaga
 */
public class AccountFragment extends AbstractFragment<MainActivity> {
    private static final String TAG = AccountFragment.class.getSimpleName();
    @BindView(R.id.listView1)
    ListView accountListView;
    @BindView(R.id.tv_account)
    TextView tvAccount;

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_account;
    }

    @Override
    protected void initView(View view) {
//        View footerView = getActivity().getLayoutInflater().inflate(
//                R.layout.account_footer, accountListView, false);
//        Button buttonNextContest = (Button) footerView.findViewById(R.id.btn_next_contest);
//        buttonNextContest.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(getActivity(), R.string.this_feature_is_coming_soon, Toast.LENGTH_LONG).show();
//            }
//        });
//        accountListView.addFooterView(footerView, null, false);
        accountListView.setAdapter(new AccountAdapter(getActivity(), R.layout.item_account, getResources().getStringArray(R.array.account_list)));
        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (position) {
//                    case 1:
//                        TransitionsManager.getInstance().replaceFragment(Screen.JOIN_STORY);
//                        break;
                    default:
                        Toast.makeText(getActivity(), R.string.this_feature_is_coming_soon, Toast.LENGTH_LONG).show();
                        break;
                }
            }
        };
        accountListView.setOnItemClickListener(itemClickListener);
        User user = getBaseActivity().getUser();
        if (user != null) {
            String fullName = String.format(Locale.US, getResources().getString(R.string.account_name), user.getFullName());
            tvAccount.setText(fullName);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getBaseActivity().getToolbarController().setTitle(getResources().getString(R.string.fragment_account_title));
        Integer points = getBaseActivity().getUser().getPointBalance();
        String pointsStr = String.format(Locale.US, getResources().getString(R.string.header_hub_points), points);
        getBaseActivity().getToolbarController().setRightText(pointsStr);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }
}
