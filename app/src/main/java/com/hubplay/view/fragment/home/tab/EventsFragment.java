package com.hubplay.view.fragment.home.tab;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hubplay.R;
import com.hubplay.TheApplication;
import com.hubplay.controller.Screen;
import com.hubplay.controller.TransitionsManager;
import com.hubplay.network.BaseHandler;
import com.hubplay.network.BaseRemoteListener;
import com.hubplay.network.model.ErrorModel;
import com.hubplay.network.model.response.CommonEventsModel;
import com.hubplay.network.model.response.Event;
import com.hubplay.utils.BubbleDrawable;
import com.hubplay.utils.Constant;
import com.hubplay.utils.HelpUtils;
import com.hubplay.view.activity.MainActivity;
import com.hubplay.view.adapter.recycler.OnLoadMoreListener;
import com.hubplay.view.adapter.recycler.RecyclerViewFooterAdapter;

import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * 15.07.16.
 *
 * @author Alexey Vereshchaga
 */
public abstract class EventsFragment extends Fragment {
    private static final String TAG = EventsFragment.class.getSimpleName();
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout refreshLayout;

    protected RecyclerViewFooterAdapter recyclerAdapter;
    protected BaseHandler<CommonEventsModel, ErrorModel> callback;
    protected Integer page = 0;
    protected Integer sizeOfPage;
    protected Integer totalPages;

    protected abstract int getViewLayout();

    protected abstract void getEventsList();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getViewLayout(), container, false);
        ButterKnife.bind(this, view);
        initView();
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MainActivity activity = ((MainActivity) getActivity());
        Integer points = activity.getUser().getPointBalance();
        String pointsStr = String.format(Locale.US, getResources().getString(R.string.header_hub_points), points);
        activity.getToolbarController().setRightText(pointsStr);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).getToolbarController().setTitle(getResources().getString(R.string.home_choose_story));
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    protected void initView() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerAdapter = new RecyclerViewFooterAdapter(recyclerView, new ArrayList<Event>(), new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (!recyclerAdapter.loading
                        && recyclerAdapter.getItemCount() + sizeOfPage > (page + 1) * sizeOfPage
                        && page + 1 < totalPages) {
                    recyclerAdapter.addItem(null);
                    page++;
                    getEventsList();
                }
            }
        });
        recyclerAdapter.setClickListener(new RecyclerViewFooterAdapter.OnItemViewClickListener() {
            @Override
            public void onViewClick(View view, int position) {
                switch (view.getId()) {
                    case R.id.ib_play:
                        Log.d(TAG, "onViewClick: ");
                        Bundle bundle = new Bundle();
                        String questionsStr = HelpUtils.createStringFromObject(recyclerAdapter.getItem(position));
                        bundle.putString(Constant.EVENT_KEY, questionsStr);
                        TransitionsManager.getInstance().replaceFragment(Screen.CHOOSE_TOOL, bundle);
                        break;
                    case R.id.btn_more:
                        Event event = recyclerAdapter.getItem(position);
                        showPopup(getActivity(), view, event.getTitle(), event.getDescription(), view.getWidth(), view.getHeight());
                        break;
                }
            }
        });
        recyclerView.setAdapter(recyclerAdapter);
        getEvents();
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                page = 0;
                getEvents();
            }
        });
    }

    private void getEvents() {
        callback = new BaseHandler<>(new BaseRemoteListener<CommonEventsModel, ErrorModel>(getActivity()) {
            @Override
            public void onStartTask() {
                refreshLayout.setRefreshing(true);
            }

            @Override
            public void onSuccess(CommonEventsModel result) {
                if (result != null) {
                    sizeOfPage = result.getSize();
                    totalPages = result.getTotalPages();
                    if (result.getContent() != null) {
                        if (page > 0) {
                            recyclerAdapter.addItems(result.getContent());
                        } else {
                            recyclerAdapter.resetItems(result.getContent());
                        }
                    }
                }
            }

            @Override
            public void onFinishTask() {
                refreshLayout.setRefreshing(false);
                recyclerAdapter.removeItem(null);
            }
        }, new TypeReference<ErrorModel>() {
        });
        getEventsList();
    }


    private void showPopup(final FragmentActivity context, View anchorView, String title, String description, int offsetX, int offsetY) {
        if (title != null || description != null) {
            Point point = HelpUtils.getLocationOfView(anchorView);
            View layout = preparePopupView(context, title, description);

            int pointerColor = description == null
                    ? ContextCompat.getColor(TheApplication.getInstance(), R.color.popup_title)
                    : Color.BLACK;

            BubbleDrawable drawable = new BubbleDrawable(
                    BubbleDrawable.PointerAlignment.BOTTOM,
                    Color.BLACK,
                    pointerColor,
                    ContextCompat.getColor(TheApplication.getInstance(), R.color.popup_border_color));
            //
            layout.setBackground(drawable);
            // for correct measuring layout need set it width, depends from parent width
            int maxLayoutWidth = recyclerView.getWidth() - (point.x + offsetX);
            //check layout position
            layout.measure(View.MeasureSpec.makeMeasureSpec(maxLayoutWidth, View.MeasureSpec.AT_MOST),
                    View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            int measuredHeight = layout.getMeasuredHeight();
            int measuredWidth = layout.getMeasuredWidth();
            Rect location = new Rect();
            location.left = point.x + offsetX;
            int popupWidth = maxLayoutWidth > measuredWidth
                    ? WindowManager.LayoutParams.WRAP_CONTENT : measuredWidth;

            if (getRelativeToRecyclerTop(anchorView) >= measuredHeight) {
                location.bottom = point.y - measuredHeight;
            } else {
                pointerColor = ContextCompat.getColor(TheApplication.getInstance(), R.color.popup_title);
                drawable = new BubbleDrawable(
                        BubbleDrawable.PointerAlignment.TOP,
                        Color.BLACK,
                        pointerColor,
                        ContextCompat.getColor(TheApplication.getInstance(), R.color.popup_border_color));
                View titleView = layout.findViewById(R.id.tv_popup_title);
                ViewGroup.MarginLayoutParams p = (ViewGroup.MarginLayoutParams) titleView.getLayoutParams();
                p.setMargins(2, 0, 2, 2);
                titleView.requestLayout();
                layout.setBackground(drawable);
                location.bottom = point.y + offsetY;
            }

            // Creating the PopupWindow
            final PopupWindow popup = new PopupWindow(context);
            popup.setContentView(layout);
            popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);
            popup.setWidth(popupWidth);
            popup.setFocusable(true);
            popup.setOutsideTouchable(true);
            // Clear the default translucent background
            popup.setBackgroundDrawable(new BitmapDrawable());
            // Displaying the popup at the specified location, + offsets.
            popup.showAtLocation(layout, Gravity.LEFT | Gravity.TOP, location.left, location.bottom);
        }
    }

    @NonNull
    private View preparePopupView(FragmentActivity context, String title, String description) {
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popup, null);
        TextView tvTitle = (TextView) layout.findViewById(R.id.tv_popup_title);
        TextView tvDescription = (TextView) layout.findViewById(R.id.tv_popup_description);
        if (description != null) {
            tvDescription.setText(description);
        } else {
            tvDescription.setVisibility(View.GONE);
        }
        tvTitle.setText(title);
        return layout;
    }

    private int getRelativeToRecyclerTop(View myView) {
        if (myView.getParent() == recyclerView)
            return myView.getTop();
        else
            return myView.getTop() + getRelativeToRecyclerTop((View) myView.getParent());
    }

    public RecyclerViewFooterAdapter getRecyclerAdapter() {
        return recyclerAdapter;
    }
}
