package com.hubplay.view.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hubplay.R;
import com.hubplay.controller.Screen;
import com.hubplay.controller.TransitionsManager;
import com.hubplay.view.activity.StartActivity;
import com.hubplay.view.adapter.TutorialAdapter;

import java.util.Arrays;
import java.util.List;

import butterknife.BindView;

/**
 * 20.04.16.
 *
 * @author Alexey Vereshchaga
 */
public class TutorialFragment extends AbstractFragment<StartActivity> implements ViewPager.OnPageChangeListener, View.OnClickListener {

    public static final int OFFSCREEN_PAGE_LIMIT = 2;
    private TutorialAdapter mAdapter;
    private int dotsCount;
    private ImageView[] dots;
    private List<TutorialAdapter.Sheet> sheets;

    @BindView(R.id.vp_tutorial)
    ViewPager viewPager;
    @BindView(R.id.ll_indicator)
    LinearLayout llIndicator;
    @BindView(R.id.btn_sign_up)
    Button btnSignUp;
    @BindView(R.id.tv_sign_in)
    TextView tvSignIn;

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_tutorial;
    }

    @Override
    protected void initView(View view) {
        String[] stringsByPageNumbers = getResources().getStringArray(R.array.tutorial_description_list);
        sheets = Arrays.asList(
                new TutorialAdapter.Sheet(R.drawable.tutorial_1_bg, R.drawable.tutorial_1_ic, stringsByPageNumbers[0]),
                new TutorialAdapter.Sheet(R.drawable.tutorial_2_bg, R.drawable.tutorial_2_ic, stringsByPageNumbers[1]),
                new TutorialAdapter.Sheet(R.drawable.tutorial_3_bg, R.drawable.tutorial_3_ic, stringsByPageNumbers[2]),
                new TutorialAdapter.Sheet(R.drawable.tutorial_4_bg, R.drawable.tutorial_4_ic, stringsByPageNumbers[3]));
        mAdapter = new TutorialAdapter(getActivity(), sheets);
        viewPager.setAdapter(mAdapter);
        viewPager.addOnPageChangeListener(this);
        //todo hack for infinite carousel
        setUiPageViewController();
        viewPager.setCurrentItem(TutorialAdapter.MAX_COUNT / 2);
        viewPager.setOffscreenPageLimit(OFFSCREEN_PAGE_LIMIT);

        tvSignIn.setText(createSignInSpannableString(getResources().getString(R.string.tutorial_sign_in)));
        tvSignIn.setMovementMethod(LinkMovementMethod.getInstance());
        tvSignIn.setHighlightColor(Color.TRANSPARENT);

        btnSignUp.setOnClickListener(this);
    }

    private SpannableString createSignInSpannableString(String signInString) {
        int START_OF_CLICKABLE = 25;
        int END_OF_CLICKABLE = 32;

        SpannableString ss = new SpannableString(signInString);
        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
//                Bundle args = new Bundle();
//                args.putBoolean(SignUpFragment.SIGN_UP_KEY, false);
//                TransitionsManager.getInstance().replaceFragment(Screen.SIGN_UP, args);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setUnderlineText(false);
            }
        };
        ss.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.common_blue_1)),
                START_OF_CLICKABLE, END_OF_CLICKABLE, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(clickableSpan, START_OF_CLICKABLE, END_OF_CLICKABLE, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(new RelativeSizeSpan(1.4f), START_OF_CLICKABLE, END_OF_CLICKABLE, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return ss;

    }

    private void setUiPageViewController() {
        int dotMargin = (int) getResources().getDimension(R.dimen.tutorial_dot_margin);
        dotsCount = sheets.size();
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(getActivity());
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.tutorial_dot_nonselected));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(dotMargin, 0, dotMargin, 0);
            llIndicator.addView(dots[i], params);
        }
        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.tutorial_dot_selected));
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        int virtualPosition = position % dotsCount;
        for (int i = 0; i < dotsCount; i++) {
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.tutorial_dot_nonselected));
        }
        dots[virtualPosition].setImageDrawable(getResources().getDrawable(R.drawable.tutorial_dot_selected));
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_sign_up:
//                Bundle args = new Bundle();
//                args.putBoolean(SignUpFragment.SIGN_UP_KEY, true);
//                TransitionsManager.getInstance().replaceFragment(Screen.SIGN_UP, args);
                break;
        }
    }
}
