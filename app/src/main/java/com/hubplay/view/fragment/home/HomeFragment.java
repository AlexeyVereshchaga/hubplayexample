package com.hubplay.view.fragment.home;

import android.content.pm.ActivityInfo;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.hubplay.R;
import com.hubplay.view.activity.MainActivity;
import com.hubplay.view.adapter.HomePagerAdapter;
import com.hubplay.view.fragment.AbstractFragment;

import butterknife.BindView;

/**
 * 11.07.16.
 *
 * @author Alexey Vereshchaga
 */
public class HomeFragment extends AbstractFragment<MainActivity> {
    @BindView(R.id.vp_home)
    ViewPager vpHome;
    @BindView(R.id.pager_tab_strip)
    PagerTabStrip pagerTabStrip;

    private HomePagerAdapter pagerAdapter;

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_home;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((MainActivity) getActivity()).getToolbarController().setTitle(getResources().getString(R.string.home_choose_story));
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }

    @Override
    protected void initView(View view) {
//        if (pagerAdapter == null) {
            pagerAdapter = new HomePagerAdapter(getChildFragmentManager(), getActivity());
//        }
        vpHome.setAdapter(pagerAdapter);
        pagerTabStrip.setTabIndicatorColor(getResources().getColor(R.color.common_blue_1));
    }

    public HomePagerAdapter getPagerAdapter() {
        return pagerAdapter;
    }
}
