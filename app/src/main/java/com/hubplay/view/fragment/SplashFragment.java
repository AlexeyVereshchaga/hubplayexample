package com.hubplay.view.fragment;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.ProgressBar;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hubplay.R;
import com.hubplay.controller.DataManager;
import com.hubplay.controller.Screen;
import com.hubplay.controller.TransitionsManager;
import com.hubplay.network.BaseHandler;
import com.hubplay.network.BaseRemoteListener;
import com.hubplay.network.model.ErrorModel;
import com.hubplay.network.model.response.Access;
import com.hubplay.network.model.response.User;
import com.hubplay.utils.Constant;
import com.hubplay.utils.HelpUtils;
import com.hubplay.utils.SharedPreferenceHelper;
import com.hubplay.view.activity.MainActivity;
import com.hubplay.view.activity.StartActivity;

import java.util.Date;

import butterknife.BindView;

/**
 * 15.06.16.
 *
 * @author Alexey Vereshchaga
 */
public class SplashFragment extends AbstractFragment<StartActivity> {

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_splash;
    }

    @Override
    protected void initView(View view) {
        DataManager.refreshToken(new BaseHandler<>(new BaseRemoteListener<Access, ErrorModel>(getActivity()) {
            @Override
            public void onStartTask() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(Access result) {
                SharedPreferenceHelper.saveAccessToken(result.getAccessToken());
                SharedPreferenceHelper.saveRefreshToken(result.getRefreshToken());
                SharedPreferenceHelper.saveTokenType(result.getTokenType());
                SharedPreferenceHelper.saveExpiredAt(new Date().getTime() + result.getExpiresIn() * DateUtils.SECOND_IN_MILLIS);
                getUser();
            }

            @Override
            public void onError(Throwable t) {
                restartSignUpFragment();
            }

            @Override
            public void onFinishTask() {
                progressBar.setVisibility(View.GONE);
            }
        }, new TypeReference<ErrorModel>() {
        }));
    }

    private void getUser() {
        DataManager.getUser(new BaseHandler<>(new BaseRemoteListener<User, ErrorModel>(getActivity()) {
            @Override
            public void onStartTask() {
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onSuccess(User result) {
                Bundle bundle = new Bundle();
                bundle.putString(Constant.USER, HelpUtils.createStringFromObject(result));
                TransitionsManager.getInstance().startActivity(MainActivity.class, bundle);
            }

            @Override
            public void onError(Throwable t) {
                super.onError(t);
                restartSignUpFragment();
            }

            public void onFinishTask() {
                progressBar.setVisibility(View.GONE);
            }
        }, new TypeReference<ErrorModel>() {
        }));
    }

    private void restartSignUpFragment() {
        HelpUtils.deleteTokens();
        getActivity().getSupportFragmentManager().beginTransaction().remove(SplashFragment.this).commit();
        TransitionsManager.getInstance().addFragment(Screen.LOGIN);
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().hide();
    }
}
