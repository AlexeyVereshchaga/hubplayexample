package com.hubplay.view.fragment.login;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.hubplay.R;
import com.hubplay.controller.Screen;
import com.hubplay.controller.TransitionsManager;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 06.07.16.
 *
 * @author Alexey Vereshchaga
 */
public class HostLoginFragment extends LoginFragment {

    @BindView(R.id.et_contest_id)
    EditText etContestId;

    @Override
    protected void initView(View view) {
        super.initView(view);
        btnRegister.setText(R.string.host_login_host_register);
        tvLogin.setText(R.string.host_login_login);
        etContestId.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.btn_login_request)
    void onClickLogin() {
        Toast.makeText(getActivity(), R.string.this_feature_is_coming_soon, Toast.LENGTH_LONG).show();
    }

    @OnClick(R.id.btn_register_switch)
    void onClickHostRegister() {
        Bundle bundle = new Bundle();
        bundle.putBoolean(HostRegistrationFragment.HOST_LOGIN_FLAG_KEY, true);
        TransitionsManager.getInstance().replaceFragment(Screen.HOST_REGISTRATION, bundle);
    }

    @OnClick(R.id.tv_login)
    void onClickLink() {
        getActivity().onBackPressed();
    }
}
