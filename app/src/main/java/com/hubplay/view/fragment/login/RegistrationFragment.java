package com.hubplay.view.fragment.login;

import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hubplay.R;
import com.hubplay.controller.DataManager;
import com.hubplay.controller.Screen;
import com.hubplay.controller.TransitionsManager;
import com.hubplay.network.BaseHandler;
import com.hubplay.network.BaseRemoteListener;
import com.hubplay.network.model.ErrorModel;
import com.hubplay.utils.HelpUtils;
import com.hubplay.view.activity.StartActivity;
import com.hubplay.view.fragment.AbstractFragment;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * 06.07.16.
 *
 * @author Alexey Vereshchaga
 */
public class RegistrationFragment extends AbstractFragment<StartActivity> {
    @BindView(R.id.et_first_last_name)
    EditText etFirstLastName;
    @BindView(R.id.et_username)
    EditText etUsername;
    @BindView(R.id.et_email)
    EditText etEmail;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.et_repeat_password)
    EditText etRepeatPassword;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_registration;
    }

    @Override
    public void onResume() {
        super.onResume();
        getBaseActivity().getToolbarController().setTitle(getResources().getString(R.string.sign_up));
    }

    @OnClick(R.id.btn_register_request)
    void register() {
        int invalidFieldMessageId = getInvalidFieldMessageId();
        if (invalidFieldMessageId != -1) {
            Toast.makeText(getActivity(), invalidFieldMessageId, Toast.LENGTH_LONG).show();
            return;
        }
        DataManager.register(etFirstLastName.getText().toString(),
                etUsername.getText().toString(),
                etEmail.getText().toString(),
                etPassword.getText().toString(),
                new BaseHandler<>(new BaseRemoteListener<Void, ErrorModel>(getActivity()) {
                    @Override
                    public void onStartTask() {
                        progressBar.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onSuccess(Void result) {
                        if (getActivity() != null) {
                            hideKeyboard();
                            //// TODO: 06.07.16 temporary message
                            Toast.makeText(getActivity(), "Registered successfully!", Toast.LENGTH_LONG).show();
                        }
                    }

                    @Override
                    public void onFinishTask() {
                        progressBar.setVisibility(View.GONE);
                    }
                }, new TypeReference<ErrorModel>() {
                }));
    }

    @OnClick(R.id.btn_host_register_switch)
    void onClickHostRegister() {
        TransitionsManager.getInstance().replaceFragment(Screen.HOST_REGISTRATION);
    }

    private int getInvalidFieldMessageId() {
        return HelpUtils.validateFields(etFirstLastName.getText().toString(),
                etUsername.getText().toString(),
                etEmail.getText().toString(),
                etPassword.getText().toString(),
                etRepeatPassword.getText().toString());
    }
}
