package com.hubplay.view.fragment.dialog;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;

import com.hubplay.R;
import com.hubplay.utils.SharedPreferenceHelper;
import com.hubplay.view.fragment.EventFragment;

/**
 * @author Alexey Vereshchaga
 */
public class EventDialog extends DialogFragment {

    private static final String TITLE = "title";
    private static final String MESSAGE = "message";
    private static final String DIALOG_TYPE = "dialog_type";
    private DialogInterface.OnClickListener onClickListener;

    public static EventDialog newInstance(String title, String message, DialogType dialogType, DialogInterface.OnClickListener onClickListener) {
        EventDialog dialog = new EventDialog();
        Bundle args = new Bundle();
        args.putString(TITLE, title);
        args.putString(MESSAGE, message);
        args.putString(DIALOG_TYPE, dialogType.name());
        dialog.setArguments(args);
        dialog.setOnClickListener(onClickListener);
        return dialog;
    }

    @NonNull
    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
        String title = getArguments().getString(TITLE);
        String message = getArguments().getString(MESSAGE);
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),
                android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar);
        DialogType dialogType;
        dialogType = DialogType.valueOf(getArguments().getString(DIALOG_TYPE));
        int positiveButtonTextId = 0;
        if (savedInstanceState != null) {
            EventFragment fragment = (EventFragment) getFragmentManager().findFragmentByTag(EventFragment.class.getName());
            if (fragment != null) {
                onClickListener = fragment.getOnClickListener();
            }
        }
        switch (dialogType) {
            case OK:
                positiveButtonTextId = R.string.dialog_ok;
                break;
            case RETRY:
                positiveButtonTextId = R.string.dialog_cancel;
                builder.setNegativeButton(R.string.dialog_retry, onClickListener);
                break;
        }
        builder.setPositiveButton(positiveButtonTextId, onClickListener);
        builder.setTitle(title);
        builder.setMessage(message);
        return builder.create();
    }

    @Override
    public void show(FragmentManager manager, String tag) {
        if (!SharedPreferenceHelper.isEventDialogShow()) {
            SharedPreferenceHelper.markEventDialogShown();
            super.show(manager, tag);
        }
    }

    private void setOnClickListener(DialogInterface.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @Override
    public void onStop() {
        super.onStop();
        SharedPreferenceHelper.markEventDialogNotShown();
    }

    public enum DialogType {
        OK, RETRY
    }
}
