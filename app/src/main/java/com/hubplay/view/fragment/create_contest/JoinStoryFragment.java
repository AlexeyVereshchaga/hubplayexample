package com.hubplay.view.fragment.create_contest;

import android.graphics.Color;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.View;
import android.widget.TextView;

import com.hubplay.R;
import com.hubplay.view.activity.MainActivity;
import com.hubplay.view.adapter.CreateContestAdapter;
import com.hubplay.view.fragment.AbstractFragment;

import butterknife.BindView;

/**
 * 03.06.16.
 *
 * @author Alexey Vereshchaga
 */
public class JoinStoryFragment extends AbstractFragment<MainActivity> {
    private static final int CURRENT_ITEM = 1;
    @BindView(R.id.vp_create_contest)
    ViewPager vpCreateContest;
    //    @BindView(R.id.tv_shows)
//    TextView tvShows;
    @BindView(R.id.tv_games)
    TextView tvGames;
//    @BindView(R.id.pager_header)
//    PagerTitleStrip ptsHeader;

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_create_contest;
    }

    @Override
    protected void initView(View view) {
        vpCreateContest.setAdapter(new CreateContestAdapter(getChildFragmentManager(), getActivity()));
        vpCreateContest.setCurrentItem(CURRENT_ITEM);
        vpCreateContest.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                float selectedTextSize = getContext().getResources().getDimension(R.dimen.create_contest_selected_text_size);
                float inactiveTextSize = getContext().getResources().getDimension(R.dimen.create_contest_inactive_text_size);
                int inactiveColor = getContext().getResources().getColor(R.color.new_contest_inactive_header);
                switch (position) {
                    case 0:
//                        tvShows.setTextSize(TypedValue.COMPLEX_UNIT_PX, selectedTextSize);
//                        tvShows.setTextColor(Color.WHITE);
                        tvGames.setTextSize(TypedValue.COMPLEX_UNIT_PX, inactiveTextSize);
                        tvGames.setTextColor(inactiveColor);
                        break;
                    case 1:
                        tvGames.setTextSize(TypedValue.COMPLEX_UNIT_PX, selectedTextSize);
                        tvGames.setTextColor(Color.WHITE);
//                        tvShows.setTextSize(TypedValue.COMPLEX_UNIT_PX, inactiveTextSize);
//                        tvShows.setTextColor(inactiveColor);
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getBaseActivity().getToolbarController().setTitle(getResources().getString(R.string.fragment_join_story));
    }

}
