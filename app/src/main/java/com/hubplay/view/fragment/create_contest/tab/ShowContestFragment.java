package com.hubplay.view.fragment.create_contest.tab;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hubplay.R;
import com.hubplay.view.activity.MainActivity;

import java.util.Locale;

import butterknife.ButterKnife;

/**
 * 03.06.16.
 *
 * @author Alexey Vereshchaga
 */
public class ShowContestFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getViewLayout(), container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    protected int getViewLayout() {
        return R.layout.fragment_leagues;
    }


    @Override
    public void onResume() {
        super.onResume();
        MainActivity activity = (MainActivity) getActivity();
        activity.getToolbarController().setTitle(getResources().getString(R.string.fragment_join_story));
        Integer points = activity.getUser().getPointBalance();
        String pointsStr = String.format(Locale.US, getResources().getString(R.string.header_hub_points), points);
        activity.getToolbarController().setRightText(pointsStr);
    }
}
