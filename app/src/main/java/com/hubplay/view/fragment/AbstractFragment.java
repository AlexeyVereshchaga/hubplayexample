package com.hubplay.view.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import com.hubplay.R;
import com.hubplay.view.activity.BaseActivity;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 20.04.16.
 *
 * @author Alexey Vereshchaga
 */
public abstract class AbstractFragment<T extends BaseActivity> extends Fragment {
    private T baseActivity;
    private Unbinder unbinder;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setBackgroundDrawableResource(R.color.common_app_bg);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getViewLayout(), container, false);
        unbinder = ButterKnife.bind(this, view);
        initView(view);
        return view;
    }

    protected abstract int getViewLayout();

    protected void initView(View view) {
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        baseActivity = (T) activity;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity) getActivity()).getSupportActionBar().show();
        getBaseActivity().getToolbarController().setTitle("");
        getBaseActivity().getToolbarController().setRightText("");
    }

    @Override
    public void onPause() {
        super.onPause();
        hideKeyboard();
    }

    protected void hideKeyboard() {
        if (getActivity().getCurrentFocus() != null) {
            Object inputMethodManager = getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            if (inputMethodManager != null) {
                ((InputMethodManager) inputMethodManager).hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public T getBaseActivity() {
        return baseActivity;
    }
}
