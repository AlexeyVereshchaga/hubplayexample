package com.hubplay.view.fragment;

import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.ListView;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hubplay.R;
import com.hubplay.controller.DataManager;
import com.hubplay.network.BaseHandler;
import com.hubplay.network.BaseRemoteListener;
import com.hubplay.network.model.ErrorModel;
import com.hubplay.network.model.response.Reward;
import com.hubplay.view.activity.MainActivity;
import com.hubplay.view.adapter.RewardAdapter;

import java.util.List;

import butterknife.BindView;

/**
 * 24.06.16.
 *
 * @author Alexey Vereshchaga
 */
public class RewardsFragment extends AbstractFragment<MainActivity> {
    @BindView(android.R.id.list)
    ListView lvEvents;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swlRewards;
    private RewardAdapter adapter;
    private List<Reward> rewards;

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_rewards;
    }

    @Override
    protected void initView(View view) {
        adapter = new RewardAdapter(getActivity(), R.layout.item_reward);
        lvEvents.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        lvEvents.setEmptyView(view.findViewById(android.R.id.empty));
        lvEvents.setAdapter(adapter);
        if (rewards != null) {
            adapter.clear();
            adapter.addAll(rewards);
        }
        swlRewards.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getRewards();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        getRewards();
        getBaseActivity().getToolbarController().setTitle(getResources().getString(R.string.rewards_title));
    }

    private void getRewards() {
        DataManager.getRewards(new BaseHandler<>(new BaseRemoteListener<List<Reward>, ErrorModel>(getActivity()) {

            @Override
            public void onStartTask() {
                if (swlRewards != null) {
                    swlRewards.setRefreshing(true);
                }
            }

            @Override
            public void onSuccess(List<Reward> result) {
                super.onSuccess(result);
                rewards = result;
                if (adapter != null) {
                    adapter.clear();
                    adapter.addAll(rewards);
                }
            }

            @Override
            public void onFinishTask() {
                if (swlRewards != null) {
                    swlRewards.setRefreshing(false);
                }
            }
        }, new TypeReference<ErrorModel>() {
        }));
    }
}
