package com.hubplay.view.fragment;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.hubplay.R;
import com.hubplay.controller.Screen;
import com.hubplay.controller.ToolbarController;
import com.hubplay.controller.TransitionsManager;
import com.hubplay.network.model.response.Event;
import com.hubplay.utils.Constant;
import com.hubplay.utils.HelpUtils;
import com.hubplay.view.activity.MainActivity;

import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

public class ChooseFragment extends AbstractFragment<MainActivity> {

    private static final String TAG = ChooseFragment.class.getSimpleName();
    @BindView(R.id.ll_trivia)
    LinearLayout llTrivia;
    @BindView(R.id.ll_action)
    LinearLayout llAction;
    @BindView(R.id.ll_watch)
    LinearLayout llWatch;
    @BindView(R.id.iv_watch)
    ImageView ivWatch;
    @BindView(R.id.iv_choose_tool_back)
    ImageView ivBackground;
    @BindView(R.id.iv_big_wheel)
    ImageView ivBigWheel;
    @BindView(R.id.tv_choose_tool)
    TextView tvChooseTool;
    private Event event;

    @Override
    protected int getViewLayout() {
        return R.layout.fragment_choose_tool;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        event = HelpUtils.readStringToObject(getArguments().getString(Constant.EVENT_KEY), Event.class);
    }

    @Override
    public void onResume() {
        super.onResume();
        ToolbarController toolbarController = getBaseActivity().getToolbarController();
        toolbarController.setTitle(getResources().getString(R.string.events_choose_tool));
        Integer points = getBaseActivity().getUser().getPointBalance();
        String pointsStr = String.format(Locale.US, getResources().getString(R.string.header_hub_points), points);
        toolbarController.setRightText(pointsStr);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    @Override
    protected void initView(View view) {
        RequestManager requestManager = Glide.with(this);
        requestManager
                .load(event.getBgIcon())
                .centerCrop()
                .into(ivBackground);

        int measureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        llAction.measure(measureSpec, measureSpec);
        llTrivia.measure(measureSpec, measureSpec);

        boolean enabled = event.getWatches() != null && !event.getWatches().isEmpty();
        llWatch.setEnabled(enabled);
        ivWatch.setEnabled(enabled);

        tvChooseTool.measure(measureSpec, measureSpec);
        int measuredHeight = llAction.getMeasuredHeight();
        int measuredWidthTrivia = llTrivia.getMeasuredWidth();
        int measuredWidthChooseTool = tvChooseTool.getMeasuredWidth();

        ivBigWheel.getLayoutParams().height = measuredHeight + getResources().getDimensionPixelOffset(R.dimen.choose_tool_top_button_margin);
        ivBigWheel.getLayoutParams().width = 2 * measuredWidthTrivia
                + measuredWidthChooseTool
                + 2 * getResources().getDimensionPixelOffset(R.dimen.choose_tool_button_size);
    }

    @OnClick(R.id.ll_trivia)
    void onClickTrivia() {
        Bundle bundle = new Bundle();
        String eventStr = HelpUtils.createStringFromObject(event);
        bundle.putString(Constant.EVENT_KEY, eventStr);
        TransitionsManager.getInstance().replaceFragment(Screen.EVENT, bundle);
    }

    @OnClick(R.id.ll_action)
    void onClickAction() {
        //// TODO: 23.07.16
    }

    @OnClick(R.id.ll_watch)
    void onClickWatch() {
        Bundle bundle = new Bundle();
        String eventStr = HelpUtils.createStringFromObject(event);
        bundle.putString(Constant.EVENT_KEY, eventStr);
        TransitionsManager.getInstance().replaceFragment(Screen.WATCH, bundle);
    }
}
