package com.hubplay.view.fragment.dialog;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import com.hubplay.R;
import com.hubplay.utils.SharedPreferenceHelper;

/**
 * 21.06.16.
 *
 * @author Alexey Vereshchaga
 */
public class OfflineDialog extends DialogFragment {

    @NonNull
    @Override
    public android.app.Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(),
                android.R.style.Theme_DeviceDefault_Light_Dialog_NoActionBar);
        builder.setPositiveButton(R.string.offline_ok, null);
        builder.setTitle(R.string.offline_title);
        builder.setMessage(R.string.offline_message);
        return builder.create();
    }

    @Override
    public void onStop() {
        super.onStop();
        SharedPreferenceHelper.markOfflineDialogNotShown(getActivity());
    }
}