package com.hubplay.view.fragment.home.tab;

import com.hubplay.R;
import com.hubplay.controller.DataManager;

/**
 * 11.07.16.
 *
 * @author Alexey Vereshchaga
 */
public class FeaturedFragment extends EventsFragment {

    protected int getViewLayout() {
        return R.layout.fragment_featured;
    }

    protected void getEventsList() {
        DataManager.getFeaturedEvents(page, callback);
    }
}
