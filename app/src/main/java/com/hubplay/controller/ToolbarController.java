package com.hubplay.controller;

import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.hubplay.R;

/**
 * 22.04.16.
 *
 * @author Alexey Vereshchaga
 */
public class ToolbarController {

    private Toolbar toolbar;
    private TextView toolbarTitle;
    private ImageButton ibLeft;
    private TextView tvRight;

    public ToolbarController(Toolbar toolbar) {
        this.toolbar = toolbar;
        toolbarTitle = (TextView) toolbar.findViewById(R.id.tv_toolbar_title);
        ibLeft = (ImageButton) toolbar.findViewById(R.id.ib_left);
        tvRight = (TextView) toolbar.findViewById(R.id.tv_right);
    }

    public Toolbar getToolbar() {
        return toolbar;
    }

    public void setToolbar(Toolbar toolbar) {
        this.toolbar = toolbar;
    }

    public TextView getToolbarTitleView() {
        return toolbarTitle;
    }

    public ImageButton getToolbarButton() {
        return ibLeft;
    }

    public TextView getRightTextView() {
        return tvRight;
    }

    public void setToolbarButton(int imageResId, View.OnClickListener onClickListener) {
        ibLeft.setImageResource(imageResId);
        if (onClickListener != null) {
            ibLeft.setOnClickListener(onClickListener);
        }
    }

    public void setTitle(String title) {
        toolbarTitle.setText(title);
    }

    public void setRightText(String text) {
        tvRight.setText(text);
    }
}
