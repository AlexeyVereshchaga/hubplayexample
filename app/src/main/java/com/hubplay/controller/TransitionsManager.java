package com.hubplay.controller;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.hubplay.R;
import com.hubplay.utils.HelpUtils;
import com.hubplay.view.activity.BaseActivity;
import com.hubplay.view.activity.StartActivity;

/**
 * 20.04.16.
 *
 * @author Alexey Vereshchaga
 */
public class TransitionsManager {

    private static volatile TransitionsManager instance;

    private FragmentManager fragmentManager;
    private BaseActivity currentActivity;

    private TransitionsManager() {
    }

    public static TransitionsManager getInstance() {
        TransitionsManager localInstance = instance;
        if (localInstance == null) {
            synchronized (TransitionsManager.class) {
                localInstance = instance;
                if (localInstance == null) {
                    instance = localInstance = new TransitionsManager();
                }
            }
        }
        return localInstance;
    }

    public <T extends BaseActivity> void init(T activity) {
        currentActivity = activity;
        fragmentManager = activity.getSupportFragmentManager();
    }


    public <T extends Fragment> T addFragment(Screen screen, Bundle args) {
        return transition(screen, args, false, TransitionType.ADD, false);
    }

    public <T extends Fragment> T addFragment(Screen screen) {
        return addFragment(screen, null);
    }

    public <T extends Fragment> T replaceFragment(Screen screen, Bundle args, boolean addToBackStack) {
        return transition(screen, args, addToBackStack, TransitionType.REPLACE, false);
    }

    public <T extends Fragment> T replaceFragment(Screen screen, Bundle args) {
        return replaceFragment(screen, args, true);
    }

    public <T extends Fragment> T replaceFragment(Screen screen) {
        return replaceFragment(screen, null);
    }

    public <T extends Fragment> T transition(Screen screen, Bundle args, boolean addToBackStack, TransitionType transitionType, boolean animate) {
        if (fragmentManager == null)
            throw new IllegalStateException("Call TransitionsManager.init()");
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        String fragmentTag = screen.getFragmentClass().getName();
        T fragment = (T) Fragment.instantiate(currentActivity, screen.getFragmentClass().getName());
        fragment.setArguments(args);

        if (animate) {
            transaction.setCustomAnimations(R.anim.fragm_in_from_right, R.anim.fragm_out_to_right);
        }
        switch (transitionType) {
            case ADD:
                transaction.add(currentActivity.getFragmentContainerId(), fragment, fragmentTag);
                break;
            case REPLACE:
                transaction.replace(currentActivity.getFragmentContainerId(), fragment, fragmentTag);
                break;
        }
        if (addToBackStack) {
            transaction.addToBackStack(screen.name());
        }
        transaction.commit();
        return fragment;

    }

    public void startActivity(Class<? extends Activity> newActivityClass, Bundle extras) {
        if (currentActivity == null)
            throw new IllegalStateException("Call TransitionsManager.init()");
        Intent startIntent = new Intent(currentActivity, newActivityClass);
        if (extras != null) {
            startIntent.putExtras(extras);
        }
        currentActivity.startActivity(startIntent);
        currentActivity.finish();
        currentActivity = null;
    }

    public void popBackStackToFirst() {
        if (fragmentManager.getBackStackEntryCount() > 0) {
            FragmentManager.BackStackEntry backStackEntry = fragmentManager.getBackStackEntryAt(0);
            fragmentManager.popBackStack(backStackEntry.getId(), android.app.FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
    }

    public static void logout(FragmentActivity activity) {
        HelpUtils.deleteTokens();
        Intent intent = new Intent(activity, StartActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    public enum TransitionType {
        ADD, REPLACE
    }
}
