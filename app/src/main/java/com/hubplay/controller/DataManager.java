package com.hubplay.controller;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;

import com.fasterxml.jackson.core.type.TypeReference;
import com.hubplay.BuildConfig;
import com.hubplay.R;
import com.hubplay.TheApplication;
import com.hubplay.network.BaseHandler;
import com.hubplay.network.HubPlayApi;
import com.hubplay.network.WrapperCall;
import com.hubplay.network.model.Action;
import com.hubplay.network.model.ErrorModel;
import com.hubplay.network.model.GrantType;
import com.hubplay.network.model.RefreshRemoteListener;
import com.hubplay.network.model.request.Registration;
import com.hubplay.network.model.response.Access;
import com.hubplay.network.model.response.CommonEventsModel;
import com.hubplay.network.model.response.Event;
import com.hubplay.network.model.response.League;
import com.hubplay.network.model.response.QuestionOrder;
import com.hubplay.network.model.response.Reward;
import com.hubplay.network.model.response.User;
import com.hubplay.utils.EventCategory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;

import static com.hubplay.utils.SharedPreferenceHelper.getAccessToken;
import static com.hubplay.utils.SharedPreferenceHelper.getRefreshToken;
import static com.hubplay.utils.SharedPreferenceHelper.getServer;
import static com.hubplay.utils.SharedPreferenceHelper.getTokenType;

/**
 * 21.04.16.
 *
 * @author Alexey Vereshchaga
 */
public class DataManager {

    private static List<WrapperCall> callsUnauthorized = new ArrayList<>();
    public static Boolean isRefreshed = false;

    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(getServer())
            .addConverterFactory(JacksonConverterFactory.create())
            .client(createNewOkHttpClient())
            .build();

    private static HubPlayApi hubPlayApi = retrofit.create(HubPlayApi.class);

    private static OkHttpClient createNewOkHttpClient() {
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        HttpLoggingInterceptor loggingInterceptor = null;
        if (BuildConfig.DEBUG) {
            loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        }
        if (loggingInterceptor != null) {
            clientBuilder.addInterceptor(loggingInterceptor);
        }
        Interceptor interceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder()
                        .addHeader("Authorization", getTokenType() + " " + getAccessToken())
                        .addHeader("Content-Type", "application/json")
                        .build();
                return chain.proceed(request);
            }
        };
        clientBuilder.addInterceptor(interceptor);
        return clientBuilder.build();
    }

    public static Call<Access> login(String username, String password, BaseHandler<Access, ErrorModel> callback) {
        callback.onStart();
        Call<Access> call = hubPlayApi.getToken(GrantType.PASSWORD.name().toLowerCase(), username,
                password,
                TheApplication.getInstance().getString(R.string.client_id),
                TheApplication.getInstance().getString(R.string.client_secret),
                null);
        call.enqueue(callback);
        return call;
    }

    public static Call<Void> forgotPassword(String email, BaseHandler<Void, ErrorModel> callback) {
        callback.onStart();
        Call<Void> call = hubPlayApi.forgotPassword(email);
        call.enqueue(callback);
        return call;
    }

    public static Call<Void> register(String firstLastName, String username, String email, String password, BaseHandler<Void, ErrorModel> callback) {
        Registration registration = new Registration(firstLastName, username, email, password);
        callback.onStart();
        Call<Void> call = hubPlayApi.register(registration);
        call.enqueue(callback);
        return call;
    }

    public static Call<User> getUser(BaseHandler<User, ErrorModel> callback) {
        callback.onStart();
        Call<User> call = hubPlayApi.getUser();
        call.enqueue(callback);
        return call;
    }

    public static Call<List<League>> getLeagues(BaseHandler<List<League>, ErrorModel> callback) {
        callback.onStart();
        Call<List<League>> call = hubPlayApi.getLeagues();
        call.enqueue(callback);
        return call;
    }

    public static Call<List<League>> getFullLeagues(BaseHandler<List<League>, ErrorModel> callback) {
        callback.onStart();
        Call<List<League>> call = hubPlayApi.getFullLeagues();
        call.enqueue(callback);
        return call;
    }

    public static Call<List<Event>> getEvents(Integer leagueId, BaseHandler<List<Event>, ErrorModel> callback) {
        callback.onStart();
        Call<List<Event>> call = hubPlayApi.getEvents(leagueId);
        call.enqueue(callback);
        return call;
    }

    public static Call<List<Event>> getEventsByDay(Integer leagueId, BaseHandler<List<Event>, ErrorModel> callback) {
        callback.onStart();
        Call<List<Event>> call = hubPlayApi.getEventsByDay(leagueId, /*need lazy load*/ 0);
        call.enqueue(callback);
        return call;
    }

    public static WrapperCall saveAnswer(Integer answerId,
                                         Integer points,
                                         Integer questionId,
                                         Integer eventId,
                                         Boolean isLast,
                                         BaseHandler<Void, ErrorModel> callback) {
        callback.onStart();
        Call<Void> call = hubPlayApi.saveGameFlow(answerId, points, questionId, eventId, isLast);
        call.enqueue(callback);
        return new WrapperCall(call, callback);
    }

    public static WrapperCall sendAction(Integer eventId, Action action, BaseHandler<QuestionOrder, ErrorModel> callback) {
        callback.onStart();
        Call<QuestionOrder> call = hubPlayApi.sendGameFlowAction(eventId, action.name());
        call.enqueue(callback);
        return new WrapperCall(call, callback);
    }

    public static Call<List<Reward>> getRewards(BaseHandler<List<Reward>, ErrorModel> callback) {
        callback.onStart();
        Call<List<Reward>> call = hubPlayApi.getRewards();
        call.enqueue(callback);
        return call;
    }

    public static Call<CommonEventsModel> getFeaturedEvents(@NonNull Integer page, BaseHandler<CommonEventsModel, ErrorModel> callback) {
        callback.onStart();
        Call<CommonEventsModel> call = hubPlayApi.getEventsByCategory(EventCategory.FEATURED.name(), page, null);
        call.enqueue(callback);
        return call;
    }

    public static Call<CommonEventsModel> getSportsEvents(Integer leagueId, @NonNull Integer page, BaseHandler<CommonEventsModel, ErrorModel> callback) {
        callback.onStart();
        Call<CommonEventsModel> call = hubPlayApi.getEventsByCategory(EventCategory.SPORTS.name(), page, leagueId);
        call.enqueue(callback);
        return call;
    }

    public static Call<Void> cleanHistory(BaseHandler<Void, ErrorModel> callback) {
        callback.onStart();
        Call<Void> call = hubPlayApi.cleanHistory();
        call.enqueue(callback);
        return call;
    }

    //refresh token part
    public static Call<Access> refreshToken(BaseHandler<Access, ErrorModel> callback) {
        callback.onStart();
        Call<Access> call = hubPlayApi.getToken(GrantType.REFRESH_TOKEN.name().toLowerCase(),
                null,
                null,
                TheApplication.getInstance().getString(R.string.client_id),
                TheApplication.getInstance().getString(R.string.client_secret),
                getRefreshToken());
        call.enqueue(callback);
        return call;
    }

    public static void refreshToken(FragmentActivity activity) {
        if (!isRefreshed) {
            refreshToken(new BaseHandler<>(new RefreshRemoteListener(activity), new TypeReference<ErrorModel>() {
            }));
        }
    }

    public static void repeatRequests() {
        if (!callsUnauthorized.isEmpty()) {
            Iterator<WrapperCall> iterator = callsUnauthorized.iterator();
            while (iterator.hasNext()) {
                WrapperCall wrapperCall = iterator.next();
                Call clonedCall = wrapperCall.getCall().clone();
                clonedCall.enqueue(wrapperCall.getHandler());
                iterator.remove();
            }
        }
    }

    public static void addCallUnauthorized(WrapperCall wrapperCall) {
        callsUnauthorized.add(wrapperCall);
    }

    public static List<WrapperCall> getCallsUnauthorized() {
        return callsUnauthorized;
    }
}
