package com.hubplay.controller;

import android.support.v4.app.Fragment;

import com.hubplay.view.fragment.AccountFragment;
import com.hubplay.view.fragment.ChooseFragment;
import com.hubplay.view.fragment.EventFragment;
import com.hubplay.view.fragment.EventsFragment;
import com.hubplay.view.fragment.ShareFragment;
import com.hubplay.view.fragment.SplashFragment;
import com.hubplay.view.fragment.TutorialFragment;
import com.hubplay.view.fragment.WatchFragment;
import com.hubplay.view.fragment.create_contest.JoinStoryFragment;
import com.hubplay.view.fragment.home.HomeFragment;
import com.hubplay.view.fragment.login.ForgotPasswordFragment;
import com.hubplay.view.fragment.login.HostLoginFragment;
import com.hubplay.view.fragment.login.HostRegistrationFragment;
import com.hubplay.view.fragment.login.LoginFragment;
import com.hubplay.view.fragment.login.RegistrationFragment;

/**
 * 20.04.16.
 *
 * @author Alexey Vereshchaga
 */
public enum Screen {
    SPLASH(SplashFragment.class),
    LOGIN(LoginFragment.class),
    REGISTRATION(RegistrationFragment.class),
    HOST_REGISTRATION(HostRegistrationFragment.class),
    FORGOT_PASSWORD(ForgotPasswordFragment.class),
    HOME(HomeFragment.class),
    TUTORIAL(TutorialFragment.class),
    ACCOUNT(AccountFragment.class),
    JOIN_STORY(JoinStoryFragment.class),
    CHOOSE_GAME(EventsFragment.class),
    EVENT(EventFragment.class),
    //    REWARDS(RewardsFragment.class),
    HOST_LOGIN(HostLoginFragment.class),
    SHARE(ShareFragment.class),
    CHOOSE_TOOL(ChooseFragment.class),
    WATCH(WatchFragment.class);

    Screen(Class<? extends Fragment> fragmentClass) {
        this.fragmentClass = fragmentClass;
    }

    private Class<? extends Fragment> fragmentClass;

    public Class<? extends Fragment> getFragmentClass() {
        return fragmentClass;
    }
}
